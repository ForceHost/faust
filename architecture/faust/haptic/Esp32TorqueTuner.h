/*
    Esp32TorqueTuner - A driver for interfacing with the haptic device
    Torquetuner on the ESP32 platform

    Copyright (C) 2020, Mathias Kirkegaard, IDMIL

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __TORQUETUNER_H
#define __TORQUETUNER_H

#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */
#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */

// #define HAPTIC_I2C_MASTER_FREQ_HZ 1000000
#define HAPTIC_I2C_MASTER_FREQ_HZ 400000
#define HAPTIC_I2C_MASTER_TIMEOUT 10000 /*!< APB cycles (80 MhZ) Default is 640 */

#ifdef LYRA_V4_3
#define HAPTIC_I2C_MASTER_SCL_IO 23
#define HAPTIC_I2C_MASTER_SDA_IO 18
#define HAPTIC_I2C_MASTER_NUM 1
#elif TSTICK_JOINT
#define HAPTIC_I2C_MASTER_SCL_IO 25
#define HAPTIC_I2C_MASTER_SDA_IO 26
#define HAPTIC_I2C_MASTER_NUM 0
#else
#define HAPTIC_I2C_MASTER_SCL_IO 22
#define HAPTIC_I2C_MASTER_SDA_IO 21
#define HAPTIC_I2C_MASTER_NUM 0
#endif

#define COMPENSATE_FRICTION false
#define BYPASS false

const int NUM_HAPTIC_DOF = 1;

#include <stdint.h>

#include <iostream>

#include "driver/i2c.h"
#include "esp_err.h"
#include "esp_log.h"
#include "faust/haptic/HapticDev.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

static const char *TAG = "TORQUETUNER";

class TorqueTuner : public HapticDev {
   public:
    const int rx_data_size = 4;
    const int tx_data_size = 4;
    uint8_t   i2c_adress;
    uint8_t   rx_data[4];
    uint8_t   tx_data[2];
    float     pos     = 0;  // [degree]
    float     vel     = 0;  // [degree/sec]
    float     force   = 0;  // [N mc]
    float *   p_pos   = &pos;
    float *   p_vel   = &vel;
    float *   p_force = &force;

    void add_position_input(FAUSTFLOAT *zone) { p_pos = zone; }
    void add_velocity_input(FAUSTFLOAT *zone) { p_vel = zone; }
    void add_force_output(FAUSTFLOAT *zone) { p_force = zone; }

    enum BOUNDARY_CONDITION {
        WRAP    = 0,
        FOLD    = 1,
        CLIP    = 2,
        UNLIMIT = 3,  // default
    };

    BOUNDARY_CONDITION boundary = WRAP;

    void update_inputs() {
        esp_err_t err;
        uint16_t  pos_tmp;
        int16_t   vel_tmp;

        do {
            err = read(rx_data, rx_data_size);
        } while (err != ESP_OK);

        // Convert and copy positon input
        memcpy(&pos_tmp, rx_data, 2);  // Try bitmask instead of memcpy for optimization
        if (pos_tmp <= 3600) {
            pos = fixed_to_float(apply_boundaries(pos_tmp));
        } else {
            std::cout << "pos recieve error" << std::endl;
        }

        // Convert and copy velocity input
        memcpy(&vel_tmp, rx_data + 2, 2);
        if (vel_tmp <= 3*3600 && vel_tmp >= -3*3600) {
            vel = fixed_to_float(gate(vel_tmp, 50, 0));
        } else {
            std::cout << "vel recieve error" << std::endl;
        }
        ui_mutex->lock_resource();
        *p_pos = pos;
        *p_vel = vel;
        ui_mutex->free_resource();
    }
    void update_outputs() {
        esp_err_t err;
        int16_t   tmp;

        ui_mutex->lock_resource();
        // Convert and write force
        #if BYPASS
        tmp = *p_force*10;
        #else
        tmp = torque_to_pulse_width(*p_force);
        #endif
        ui_mutex->free_resource();
        memcpy(tx_data, &tmp, 2);

        do {
            err = write(tx_data, tx_data_size);
        } while (err != ESP_OK);
    }
    uint8_t init(void *args) {
        esp_err_t err = ESP_OK;
        // Init i2c
        i2c_adress = *(int *)(args);
        err |= init_i2c(HAPTIC_I2C_MASTER_FREQ_HZ, HAPTIC_I2C_MASTER_SDA_IO, HAPTIC_I2C_MASTER_SCL_IO);
        if (err == ESP_OK) {
            printf("i2c initialized for torquetuner with adress %i, sda pin: %i, scl pin: %i \n", i2c_adress,
                   HAPTIC_I2C_MASTER_SDA_IO, HAPTIC_I2C_MASTER_SCL_IO);
            return 0;
        } else {
            ESP_LOGI(TAG, "I2C initialization: %s", esp_err_to_name(err));
            return 1;
        }
    }

    Esp32UiMutex *get_mutex_handle() { return ui_mutex; }

    float get_position() { return *p_pos; }

    float get_velocity() { return *p_vel; }

    void set_force(float _force) { *p_force = _force; }

    // Utils
    float apply_boundaries(uint16_t in) {
        float ret = 0;
        // The angle recieved from TorqueTuner is wrapped between [0 360]

        switch (boundary) {
            case UNLIMIT:
                ret = unwrap(in, 3600);
                break;
            case WRAP:
                ret = in;
                break;
            case FOLD:
                // To be implemented
                ret = fold(unwrap(in, 3600), 0, 3600);
                break;
            case CLIP:
                ret = clip(unwrap(in, 3600), 0, 3600);
                break;
        }
        return (float)ret;
    }

    int unwrap(int in, int range) {
        static int last       = 0;
        static int wrap_count = 0;

        int delta = in - last;
        if (abs(delta) > 100) {
            std::cout << "Delta > 100, input was:  " << in << "   last was:  " << last << std::endl;
        }
        if ((delta) < -1800) {
            wrap_count += 1;
        } else if ((delta) > 1800) {
            wrap_count -= 1;
        };
        last = in;
        return in + 3600 * wrap_count;
    }

    int mod(int in, int hi) {
        const int lo = 0;
        if (in >= hi) {
            in -= hi;
            if (in < hi) {
                return in;
            }
        } else if (in < lo) {
            in += hi;
            if (in >= lo) {
                return in;
            }
        } else {
            return in;
        }
        if (hi == lo) {
            return lo;
        }
        int c;
        c = in % hi;
        if (c < 0) {
            c += hi;
        }
        return c;
    }

    int fold(int in, int lo, int hi) {
        int b  = hi - lo;
        int b2 = b + b;
        int c  = mod(in - lo, b2);
        if (c > b) c = b2 - c;
        return c + lo;
    }

    int clip(int in, int lo, int hi) {
        if (in > hi) {
            return hi;
        } else if (in < lo) {
            return lo;
        } else {
            return in;
        }
    }

    float gate(float val, float threshold, float floor) { return abs(val) > threshold ? val : floor; };

   private:
    const float mu_v         = 0;     // viscous friction
    const float mu_c         = 0;     // coloumb friction
    const float mu_s         = 40.0;  // stiction
    const float k_m          = 208;   // torque constant [Amp / N mm]
    const float _2A_DIV_1023 = 511.5;

    esp_err_t init_i2c(int i2c_freq, int i2c_gpio_sda, int i2c_gpio_scl) {
        if (i2c_is_initialized) {
            return ESP_OK;
        } else {
            esp_err_t    err             = ESP_OK;
            i2c_port_t   i2c_master_port = (i2c_port_t)HAPTIC_I2C_MASTER_NUM;
            i2c_config_t i2c_config;
            memset(&i2c_config, 0, sizeof(i2c_config));
            i2c_config.mode             = I2C_MODE_MASTER;
            i2c_config.sda_io_num       = (gpio_num_t)i2c_gpio_sda;
            i2c_config.sda_pullup_en    = GPIO_PULLUP_ENABLE;
            i2c_config.scl_io_num       = (gpio_num_t)i2c_gpio_scl;
            i2c_config.scl_pullup_en    = GPIO_PULLUP_ENABLE;
            i2c_config.master.clk_speed = i2c_freq;
            i2c_config.clk_flags = 0;
            err |= i2c_param_config(i2c_master_port, &i2c_config);
            err |= i2c_set_timeout(i2c_master_port, HAPTIC_I2C_MASTER_TIMEOUT);  // Has to be after param_config
            // i2c_set_data_mode((i2c_port_t) HAPTIC_I2C_MASTER_NUM, (i2c_trans_mode_t) 0, (i2c_trans_mode_t) 1);
            err |= i2c_driver_install(i2c_master_port, i2c_config.mode, I2C_MASTER_RX_BUF_DISABLE,
                                      I2C_MASTER_TX_BUF_DISABLE, 0);
            if (err == ESP_OK) {
                i2c_is_initialized = true;
            }
            // err |= i2c_filter_enable(i2c_master_port, 1);
            return err;
        }
    }

    esp_err_t write(uint8_t *tx_data, uint8_t length) {
        i2c_cmd_handle_t cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (i2c_adress << 1) | I2C_MASTER_WRITE, (i2c_ack_type_t)ACK_CHECK_EN);
        i2c_master_write(cmd, tx_data, length, (i2c_ack_type_t)ACK_CHECK_EN);
        i2c_master_stop(cmd);
        esp_err_t ret = i2c_master_cmd_begin((i2c_port_t)HAPTIC_I2C_MASTER_NUM, cmd, 100 / portTICK_RATE_MS);
        i2c_cmd_link_delete(cmd);
        return ret;
    }

    esp_err_t read(uint8_t *rx_data, uint8_t length) {
        i2c_cmd_handle_t cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (i2c_adress << 1) | READ_BIT, (i2c_ack_type_t)ACK_CHECK_EN);
        if (length > 1) {
            i2c_master_read(cmd, rx_data, length - 1, (i2c_ack_type_t)ACK_VAL);
        }
        i2c_master_read_byte(cmd, rx_data + length - 1, (i2c_ack_type_t)NACK_VAL);
        i2c_master_stop(cmd);
        esp_err_t ret = i2c_master_cmd_begin((i2c_port_t)HAPTIC_I2C_MASTER_NUM, cmd, 100 / portTICK_RATE_MS);
        i2c_cmd_link_delete(cmd);
        return ret;
    }

    // This function returns the current requirred [A] to display the given torque [mNm]
    // coloumb friction is taken into account
    float torque_to_amp(int torque) {
#if COMPENSATE_FRICTION
        if (vel < 0) {
            return (torque - mu_s) / k_m;
        } else if (torque > 0) {
            return (torque + mu_s) / k_m;
        } else {
            return 0;
        }
#else
        return torque / k_m;
#endif
    }

    int16_t torque_to_pulse_width(float force) { return (int)(torque_to_amp(force) * float(_2A_DIV_1023)); }

    float fixed_to_float(int fixed_point) { return float(fixed_point) / 10.0; }
};

// }
#endif
