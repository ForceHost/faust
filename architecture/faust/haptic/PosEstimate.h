#ifndef PosEstimate_H
#define PosEstimate_H

/* --------  From Jack-qt.cpp as part of the Synth-A-Modeler commpiler by Edgar Berhdahl -------- */

class onePoleLowPass {
    double fsGuess;
    double cutoffFreq;
    double aS;
    double b;
    double aIntg;
    double prevInputSig, prevFilteredOutput;

   public:
    // When instantiating this filter, must specify cutoff frequency in Hz of
    // position estimator lowpass filter
    onePoleLowPass(double cutoffFreq) {
        fsGuess = 1000;
        aS      = 2.0 * 3.14159 * cutoffFreq;
        b       = aS / (2.0 * fsGuess + aS);
        aIntg   = (aS - 2.0 * fsGuess) / (aS + 2.0 * fsGuess);

        prevInputSig       = 0.0;
        prevFilteredOutput = 0.0;
    }

    // Receive new input sample, process it, and provide the corresponding new output sample
    inline double tick(double inputSig) {
        prevFilteredOutput = b * (inputSig + prevInputSig) - aIntg * prevFilteredOutput;
        prevInputSig       = inputSig;  // save prior input

        return (prevFilteredOutput);
    }

    inline double getMostRecentOutput(void) { return (prevFilteredOutput); }
};

// This position estimator consists simply of a lowpass filter to help suppress
// noise in the position measurement.
//
// The filter has one zero at the Nyquist frequency and one pole at cutoffFreq.
//
// Mathematically, it is described as the bilinear transform of the one-pole
// filter H(s) = aS / (s + aS).
// was 5
#define FILTERORDER 5
class positionEstimator {
    onePoleLowPass *lowPassSection[FILTERORDER];

   public:
    // When instantiating this filter, must specify cutoff frequency in Hz of
    // position estimator lowpass filter
    positionEstimator(double cutoffFreq) {
        for (int i = 0; i < FILTERORDER; i++) {
            lowPassSection[i] = new onePoleLowPass(cutoffFreq);
            cutoffFreq *= 1.7;  // 2.2 is another reasonable value
        }
    }

    // Deallocate memory when the positionEstimator object itself is deleted.
    ~positionEstimator(void) {
        for (int i = 0; i < FILTERORDER; i++) delete lowPassSection[i];
    }

    // Receive new input sample, process it, and provide the corresponding new output sample
    inline double tick(double inputSig) {
        for (int i = FILTERORDER - 1; i >= 0; i--) inputSig = lowPassSection[i]->tick(inputSig);

        return (inputSig);
    }

    inline double getMostRecentOutput(void) { return lowPassSection[0]->getMostRecentOutput(); }
};

#endif