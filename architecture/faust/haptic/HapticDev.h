/*
    HapticDev - Common class for handling haptic devices

    Copyright (C) 2020, Mathias Kirkegaard, IDMIL

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HAPTICDEV_H
#define HAPTICDEV_H

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "faust/gui/Esp32MemoryManager.h"

#define HAPTIC_TASK_PRIORITY 20
static bool i2c_is_initialized = false;

class HapticDev {
   protected:
   public:
    Esp32UiMutex* ui_mutex;
    HapticDev() {    }
    void set_mutex_handle(Esp32UiMutex * UI) {
        if (UI != NULL) {
            ui_mutex = UI;
        } else {
            std::cout << "Could not initialize mutex handle" << std::endl;
        }
    }
    virtual void    update_inputs()                      = 0;
    virtual void    update_outputs()                     = 0;
    virtual uint8_t init(void *args)                     = 0;
    virtual float   get_position()                       = 0;
    virtual float   get_velocity()                       = 0;
    virtual void    set_force(float force_)              = 0;
    virtual void    add_position_input(FAUSTFLOAT *zone) = 0;
    virtual void    add_velocity_input(FAUSTFLOAT *zone) = 0;
    virtual void    add_force_output(FAUSTFLOAT *zone)   = 0;
};

#endif