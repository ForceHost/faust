/*
    Esp32HapticController - Classes for controlling haptic devices,
    and mapping them to faust UI entries on the ESP32 platform

    Copyright (C) 2020, Mathias Kirkegaard, IDMIL

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ESP32_HAPTIC_CONTROLLER_H
#define ESP32_HAPTIC_CONTROLLER_H

#include <array>
#include <map>
#include <string>
#include <vector>

#include "faust/gui/Esp32MemoryManager.h"
#include "faust/gui/MapUI.h"
#include "faust/gui/MetaDataUI.h"
#include "faust/haptic/Esp32TorqueTuner.h"

#define HAPTIC_TASK_CORE 1

class HapticUI : public UI, public MetaDataUI, public Esp32UiMutex {
   private:
    std::string fKey, fValue, fGroup;

   public:
    typedef std::map<std::string, FAUSTFLOAT *> devZoneMap_t;
    std::map<std::string, devZoneMap_t>         fDevMap;

    HapticUI(){};
    int HAPTIC_DOFS_INITIATED = 0;

    void add_position_input() {}

    void add_velocity_input() {}

    void add_force_output() {}

    // Metadata declarations
    void declare(FAUSTFLOAT *zone, const char *key, const char *val) {
        MetaDataUI::declare(zone, key, val);
        if (std::string(key) == "style") {
            if (std::string(val) == "haptic_dev") {
                fKey   = key;
                fValue = val;
            }
        }
    }

    // clear metadata
    void closeBox() {
        fKey   = "";
        fValue = "";
        fGroup = "";
    }

    void openVerticalBox(const char *label) {
        if (fKey == "style" && fValue == "haptic_dev") {
            if (fDevMap.size() < NUM_HAPTIC_DOF) {
                fGroup = std::string(label);
                fDevMap[fGroup];
            }
        }
    }

    // Active widgets (inputs)
    void addNumEntry(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax,
                     FAUSTFLOAT step) {
        if (fValue == "haptic_dev") {
            std::string str      = std::string(label);
            fDevMap[fGroup][str] = zone;
        }
    }

    void addVerticalSlider(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax,
                           FAUSTFLOAT step) {
        addNumEntry(label, zone, init, fmin, fmax, step);
    }
    void addHorizontalSlider(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax,
                             FAUSTFLOAT step) {
        addNumEntry(label, zone, init, fmin, fmax, step);
    }

    // Passive widgets (outputs)
    void addHorizontalBargraph(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT min, FAUSTFLOAT max) {
        if (fValue == "haptic_dev") {
            fDevMap[fGroup][std::string(label)] = zone;
        }
    }

    void addVerticalBargraph(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT min, FAUSTFLOAT max) {
        addHorizontalBargraph(label, zone, min, max);
    }

    // Not implemented here
    virtual void openTabBox(const char *label) {}
    virtual void openHorizontalBox(const char *label) {}
    virtual void addButton(const char *label, FAUSTFLOAT *zone) {}
    virtual void addCheckButton(const char *label, FAUSTFLOAT *zone) {}
    virtual void addSoundfile(const char *label, const char *filename, Soundfile **sf_zone) {}
};

class HapticController {
   private:
    static void haptic_task(void *arg) {
        //const static char *TAG     = "HapticController::haptic__task";
        HapticController * haptics = static_cast<HapticController *>(arg);
        //TickType_t         xLastWakeTime;
        //const TickType_t   xFrequency = 1;
        //xLastWakeTime                 = xTaskGetTickCount();
        while (1) {
            int count = ulTaskNotifyTake(pdFALSE, 0);
            if (count > 0) {
                haptics->update();
            };
            // vTaskDelayUntil( ... );
        }
    }

   public:
    HapticUI *               hUI;
    std::vector<HapticDev *> devices;
    bool                     fRunning = false;
    TaskHandle_t             fHandle;

    HapticController() { hUI = new HapticUI(); }
    void init() {
        const static char *TAG = "HapticController::init()";
        for (auto d : hUI->fDevMap) {
            std::string device = d.first.substr(0, d.first.size() - 1);
            int         id     = d.first.back() - 48;  // convert ASCII to number
            if (device == "TorqueTuner") {
                HapticDev *dev = new TorqueTuner();
                if (hUI->get_mutex_handle() != NULL) {
                    dev->set_mutex_handle(hUI);
                }
                if (d.second.find("Pos") != d.second.end()) {
                    dev->add_position_input(d.second["Pos"]);
                    ESP_LOGI(TAG, "Pos");
                }
                if (d.second.find("Vel") != d.second.end()) {
                    dev->add_velocity_input(d.second["Vel"]);
                    ESP_LOGI(TAG, "Vel");
                }
                if (d.second.find("Force") != d.second.end()) {
                    dev->add_force_output(d.second["Force"]);
                    ESP_LOGI(TAG, "Force");
                }
                esp_err_t err = dev->init(&id);
                if (err != ESP_OK) {
                    printf("Initialization of TorqueTuner Device failed\n");
                } else {
                    devices.push_back(dev);
                }

            } else if (device == "HapticFaderBank") {
            } else if (device == "FireFader") {
            }
        }
    }

    void update_inputs() {
        for (auto dev : devices) {
            dev->update_inputs();
        }
    }

    void update_outputs() {
        for (auto dev : devices) {
            dev->update_outputs();
        }
    }

    void update() {
        update_outputs();
        update_inputs();
    }

    bool run() {
        if (!fRunning) {
            bool err               = false;
            fRunning               = true;
            const static char *TAG = "HapticController::run()";
            err |= (xTaskCreatePinnedToCore(haptic_task, "Haptic Task", 2046, (void *)this,
                                            HAPTIC_TASK_PRIORITY, &fHandle, HAPTIC_TASK_CORE) != pdPASS);
            if (err) {
                ESP_LOGI(TAG, "OBS. Haptic task did not start ! ");
                return false;
            } else {
                ESP_LOGI(TAG, "Haptic task started");
                return true;
            }
        } else {
            return true;
        }
    }

    void stop() {
        if (fRunning) {
            fRunning = false;
            vTaskDelay(1 / portTICK_PERIOD_MS);
            fHandle  = nullptr;
        }
    }
};
#endif
