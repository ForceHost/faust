#ifndef __ESP32_MEMORY_MANAGER_H
#define __ESP32_MEMORY_MANAGER_H

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

//This class implements a simple generalized API for the freeRTOS mutex

class Esp32UiMutex
{
public:
	Esp32UiMutex(){
		ui_mutex = xSemaphoreCreateMutex();
	};
	SemaphoreHandle_t ui_mutex;
	SemaphoreHandle_t get_mutex_handle() { return ui_mutex; }
	void lock_resource(){
		xSemaphoreTake(ui_mutex, portMAX_DELAY);
	}

	void free_resource(){	
		xSemaphoreGive(ui_mutex);
	}
	
};


// This class implements a safe guard for a resource
// shared between parrallel or consucative tasks in different scopes

class Esp32MutexManager {
   private:
    std::vector<Esp32UiMutex*> ui_mutexes;

   public:
    Esp32MutexManager() {}
    void add_owner(Esp32UiMutex* UI) { ui_mutexes.push_back(UI); }

    void lock_resource() { 
        for (auto m : ui_mutexes) {
            m->lock_resource();
        }
    }
    void free_resource() {
        for (auto m : ui_mutexes) {
            m->free_resource();
        }
    }
};

#endif