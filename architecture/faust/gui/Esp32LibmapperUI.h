/*
    Esp32LibmapperUI - driver for implementing a faust UI via
    libmapper


    Copyright (C) 2020, Mathias Kirkegaard, IDMIL

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ESP32_LIBMAPPER_UI_
#define ESP32_LIBMAPPER_UI_

#include "faust/gui/MapUI.h"
#include "mapper/mapper.h"
#include "faust/gui/Esp32MemoryManager.h"

const int          MPR_TASK_PRIORITY  = 10;
const int          NUM_POLLS          = 10;
const int          UPDATE_INTERVAL_MS = 10;
static const char *ESP32LibmapperUI   = "ESP32LibmapperUI";

class Esp32LibmapperUI : public MapUI, public Esp32UiMutex {
   private:
    mpr_dev      dev;
    TaskHandle_t mpr_handle;

   protected:
    std::vector<mpr_sig> mpr_out_sigs;

   public:
    Esp32LibmapperUI() {
        // Create libmpr device
        dev = mpr_dev_new("TorqueTuner", 0);

        // Create map between signals, requires the device to be initialized,
        // so poll the device until it is ready
        while (!mpr_dev_get_is_ready(dev)) {
            // ESP_LOGI(ESP32LibmapperUI, "Polling libmapper...");
            mpr_dev_poll(dev, 25);
        }
    }
    ~Esp32LibmapperUI() { stop(); }

    bool run() {
        bool ret = (xTaskCreatePinnedToCore(&mprUpdateHandler, "Libmapper task", 4096, (void *)this, MPR_TASK_PRIORITY,
                                            &mpr_handle, 0) == pdPASS);
        return ret;
    }

    bool stop() {
        mpr_dev_free(dev);
        vTaskDelete(mpr_handle);
        return true;
    }

    void update() {
        while (true) {
            // Update input signals
            for (int i = 0; i < NUM_POLLS; ++i) {
                mpr_dev_poll(dev, 0);
            }

            // Update output signals
            for (auto z : mpr_out_sigs) {
                const char *name = mpr_obj_get_prop_as_str((mpr_obj)z, MPR_PROP_NAME, NULL);
                mpr_sig_set_value(z, 0, 1, MPR_FLT, (void *)fPathZoneMap[name]);
            }
            vTaskDelay(UPDATE_INTERVAL_MS);
        }
    }

    static void mprUpdateHandler(void *arg) { static_cast<Esp32LibmapperUI *>(arg)->update(); }

    static void mpr_input_handler(mpr_sig sig, mpr_sig_evt evt, mpr_id inst, int length, mpr_type type,
                                  const void *value, mpr_time time) {
        const char *      name = mpr_obj_get_prop_as_str((mpr_obj)sig, MPR_PROP_NAME, NULL);
        Esp32LibmapperUI *obj  = static_cast<Esp32LibmapperUI *>(mpr_sig_get_inst_data(sig, inst));
        if (type == MPR_FLT) {
            obj->lock_resource();
            obj->setParamValue(name, *(float *)value);
            obj->free_resource();
            // printf("%f\n", *((float *)value));
        } else if (type == MPR_INT32) {
            obj->lock_resource();
            obj->setParamValue(name, *(int32_t *)value);
            obj->free_resource();
            // printf("%d\n", *((int32_t *)value));
        }
    }

    static void mpr_output_handler(mpr_sig sig, mpr_sig_evt evt, mpr_id inst, int length, mpr_type type,
                                   const void *value, mpr_time time) {
        // printf("%f\n", *((float *)value));
    }

    void addInputSignal(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT min, FAUSTFLOAT max, mpr_type type) {
        std::string path = buildPath(label);

        // Remove leading "/" for compatibility with libmapper name
        path.erase(0, 1);

        fPathZoneMap[path]   = zone;
        fLabelZoneMap[label] = zone;
        mpr_sig sig =
            mpr_sig_new(dev, MPR_DIR_IN, path.c_str(), 1, type, 0, 0, 0, 0, mpr_input_handler, MPR_SIG_UPDATE);
        mpr_sig_set_inst_data(sig, mpr_sig_get_newest_inst_id(sig), this);
    }

    void addOutputSignal(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax, mpr_type type) {
        std::string path = buildPath(label);

        // Remove leading "/" for compatibility with libmapper name
        path.erase(0, 1);

        fPathZoneMap[path]   = zone;
        fLabelZoneMap[label] = zone;
        mpr_sig sig =
            mpr_sig_new(dev, MPR_DIR_OUT, path.c_str(), 1, type, 0, 0, 0, 0, mpr_output_handler, MPR_SIG_UPDATE);
        mpr_out_sigs.push_back(sig);
        ESP_LOGI(ESP32LibmapperUI, "%s added, size is now: %d", path.c_str(), mpr_out_sigs.size());
        mpr_sig_set_inst_data(sig, mpr_sig_get_newest_inst_id(sig), this);
    }

    // Implementation of faust widgets with libmapper signals
    // These function will be called when running
    // DSP->buildUserInterface(Esp32LibmapperUI);

    // -- active widgets

    void addButton(const char *label, FAUSTFLOAT *zone) { addInputSignal(label, zone, 0, 1, MPR_INT32); }
    void addCheckButton(const char *label, FAUSTFLOAT *zone) { addInputSignal(label, zone, 0, 1, MPR_INT32); }

    void addVerticalSlider(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax,
                           FAUSTFLOAT step) {
        addInputSignal(label, zone, fmin, fmax, MPR_FLT);
    }
    void addHorizontalSlider(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax,
                             FAUSTFLOAT step) {
        addInputSignal(label, zone, fmin, fmax, MPR_FLT);
    }
    void addNumEntry(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax,
                     FAUSTFLOAT step) {
        addInputSignal(label, zone, fmin, fmax, MPR_FLT);
    }

    // -- passive widgets
    void addHorizontalBargraph(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax) {
        addOutputSignal(label, zone, fmin, fmax, MPR_FLT);
    }
    void addVerticalBargraph(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax) {
        addOutputSignal(label, zone, fmin, fmax, MPR_FLT);
    }
};

#endif  // ESP32_LIBMAPPER_UI_