#ifndef ESP32_WEBSOCKET_UI_
#define ESP32_WEBSOCKET_UI_

#include <stdio.h>

#include <iostream>
#include <queue>
#include <string>

#include "esp_log.h"
#include "faust/gui/MapUI.h"
#include "faust/gui/SimpleParser.h"
#include "gui_server.h"
// #include "faust/gui/meta.h"
// #include "faust/gui/MetaDataUI.h"

// #include <src/lo_types_internal.h>
// #include <src/lo_internal.h>
#include <lo/lo.h>
#include <lo/lo_cpp.h>

// TODO
// Put all tf related functions in seperate H file and implement in inherited
// class of MapUI
#define GUI_UPDATE_MS 200

using namespace std;

class TranFunc : public MapUI {
 private:
  float *data;
  string path;
  int size;
  const char *dataPath;
  const char *idxPath;
  const char *recEnablePath;
  MapUI *fUI;

  // TODO semaphores

 public:
  TranFunc(string _path, MapUI *UI) {
    fUI = UI;
    path = _path;
    cout << "Adding " << path << endl;
  }
  ~TranFunc() { delete data; }

  void updateFromGUI(int packetLength, int offset, float *dataFromGUI) {
    if (packetLength + offset * 4 <= sizeof(data[0]) * size) {
      // xSemaphoreTake(tf_data_mutex, portMAX_DELAY);
      memcpy(data + offset, dataFromGUI, packetLength);
      // xSemaphoreGive(tf_data_mutex);
    }
  }

  void updateFaustTbl() {
    static int idx = 0;
    fUI->setParamValue(recEnablePath, 0);  // Disable recording
    fUI->setParamValue(idxPath, idx);
    fUI->setParamValue(dataPath, data[idx + 1]);
    fUI->setParamValue(recEnablePath, 1);  // Disable recording
    idx++;
    if (idx > size) {
      idx = 0;
    }
  }

  void initBuffer(int _size) {
    size = _size;
    data = new float[size]();
  }

  void setDataPath(const char *path) {
    dataPath = path;
    // cout << "dataPath set" << endl;
  }

  void setIdxPath(const char *path) {
    idxPath = path;
    // cout << "idxPath set" << endl;
  }

  void setRecEnablePath(const char *path) {
    recEnablePath = path;
    fUI->setParamValue(recEnablePath, 1);  // Enable recording
    // cout << "recEnablePath set" << endl;
  }
};

struct msg_item_t {
  string path;
  double value;
};

typedef struct binMsg {
  char *data;
  uint64_t len;
} binMsg_t;

static SemaphoreHandle_t msq_queue_mutex = xSemaphoreCreateMutex();
static SemaphoreHandle_t tf_data_mutex = xSemaphoreCreateMutex();

class Esp32WebsocketUI : public MapUI, public MetaDataUI {
 private:
  // Task handles
  TaskHandle_t guiHandle = NULL;
  TaskHandle_t clientQueueHandle = NULL;
  TaskHandle_t serverHandle = NULL;
  TaskHandle_t tfHandle = NULL;
  vector<TaskHandle_t> taskList;

  const char *TEXT_MSG_RECIEVED_TAG = "Text Message Recieved Callback";
  const char *BIN_MSG_RECIEVED_TAG = "Binary Message Recieved Callback";
  const char *LO_OSC_TAG = "Liblo OSC";

  WSReciever *ws;

  static bool unpack(char *msg, msg_item_t *item) {
    const char *p = msg;
    if (!parseSQString(p, item->path)) {
      return false;
    } else {
      skipBlank(p);
      if (!parseDouble(p, item->value)) {
        return false;
      } else {
        return true;
      }
    }
  }

  // Packs path and value into the c string msg
  // Returns length of the packed string
  int pack(char *msg, string path, float value) {
    string str = "\'" + path + "\' " + to_string(value);
    strcpy(msg, str.c_str());
    return str.size();
  }

  string fKey, fValue, fLabel;
  map<string, TranFunc *> tfMap;

 public:
  queue<binMsg_t> msgQueue;
  Esp32WebsocketUI() {
    ws = new WSReciever();
    ws->setCallback(this, WEBSOCKET_TEXT, TextMsgRecievedCallbackHandler);
    ws->setCallback(this, WEBSOCKET_BIN, BinMsgRecievedCallbackHandler);
  }

  ~Esp32WebsocketUI() { stop(); }

  bool run() {
    // Start websocket server
    ws_server_start();
    taskList.push_back(guiHandle);
    taskList.push_back(clientQueueHandle);
    taskList.push_back(serverHandle);
    taskList.push_back(tfHandle);
    bool ret = (xTaskCreatePinnedToCore(&ws->client_queue_task,
                                        "client queue task", 2048, NULL, 9,
                                        &clientQueueHandle, 0) == pdPASS);
    ret |= (xTaskCreatePinnedToCore(&ws->server_task, "server_task", 8192, NULL,
                                    5, &serverHandle, 0) == pdPASS);
    ret |= (xTaskCreatePinnedToCore(&updateHandler, "Gui update Task", 4096,
                                    (void *)this, 3, &guiHandle, 0) == pdPASS);
    // ret |= (xTaskCreatePinnedToCore(&updateTranFuncHandler, "TF Update",
    // 4096, (void*)this, 1, &tfHandle, 1) == pdPASS);
    return ret;
  }

  bool stop() {
    // Stop server tasks
    for (TaskHandle_t task : taskList) {
      if (task != NULL) {
        vTaskDelete(task);
      }
    }
    // Stop websocket server
    ws_server_stop();
    return true;
  }

  // Metadata declarations
  void declare(FAUSTFLOAT *zone, const char *key, const char *val) {
    MetaDataUI::declare(zone, key, val);
    if (strcmp(key, "style") == 0) {
      if (strcmp(val, "tranfunc") == 0) {
        fKey = key;
        fValue = val;
      }
    }
  }

  // Overwrite MapUI widget initializers for integrating tf editor

  // tf layout group
  void openVerticalBox(const char *label) {
    MapUI::openVerticalBox(label);
    if (fKey == "style" && fValue == "tranfunc") {
      string path = buildPath(
          "");  // Build a path from empty string to get current group/hierachy
      path.pop_back();  // HACK : Remove "/" postfix
      addTranFunc(path);
      fLabel = path;
    }
  }

  // acces to tf data
  void addNumEntry(const char *label, FAUSTFLOAT *zone, FAUSTFLOAT init,
                   FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step) {
    if (fKey == "style" && fValue == "tranfunc") {
      TranFunc *tf = tfMap[fLabel];
      if (strcmp(label, "writeIdx") == 0) {
        tf->setIdxPath(label);
        tf->initBuffer(fmax);
      } else if (strcmp(label, "data") == 0) {
        tf->setDataPath(label);
      }
    }
    MapUI::addNumEntry(label, zone, init, fmin, fmax, step);
  }

  // display for tf rawInput
  void addHorizontalBargraph(const char *label, FAUSTFLOAT *zone,
                             FAUSTFLOAT fmin, FAUSTFLOAT fmax) {
    if (fKey == "syle" && fValue == "tranfunc") {
    }
    MapUI::addHorizontalBargraph(label, zone, fmin, fmax);
  }

  // tf record toggle
  void addButton(const char *label, FAUSTFLOAT *zone) {
    if (fKey == "style" && fValue == "tranfunc") {
      TranFunc *tf = tfMap[fLabel];
      if (strcmp(label, "recEnable") == 0) {
        tf->setRecEnablePath(label);
      }
    }
    MapUI::addButton(label, zone);
  }

  // clear metadata
  void closeBox() {
    fKey = "";
    fValue = "";
    MapUI::closeBox();
  }

  // TODO consider adding it to path/label -zone map
  void addTranFunc(string path) {
    TranFunc *tf = new TranFunc(path, this);
    tfMap[path] = tf;
  }

  // TODO - optimize so only changed values are written to tf table
  void updateTranFunc() {
    while (true) {
      for (auto &i : tfMap) {
        i.second->updateFaustTbl();
      }
      vTaskDelay(5 / portTICK_PERIOD_MS);
    }
  }

  ssize_t lo_validate_string(void *data, ssize_t size) {
    ssize_t i = 0, len = 0;
    char *pos = (char *)data;

    if (size < 0) {
      return -LO_ESIZE;  // invalid size
    }
    for (i = 0; i < size; ++i) {
      if (pos[i] == '\0') {
        len = 4 * (i / 4 + 1);
        break;
      }
    }
    if (0 == len) {
      return -LO_ETERM;  // string not terminated
    }
    if (len > size) {
      return -LO_ESIZE;  // would overflow buffer
    }
    for (; i < len; ++i) {
      if (pos[i] != '\0') {
        return -LO_EPAD;  // non-zero char found in pad area
      }
    }
    return len;
  }

  ssize_t lo_validate_bundle(void *data, ssize_t size) {
    ssize_t len = 0, remain = size;
    char *pos = (char *)data;
    ssize_t elem_len;

    len = lo_validate_string(data, size);
    if (len < 0) {
      return -LO_ESIZE;  // invalid size
    }
    if (0 != strcmp((const char *)data, "#bundle")) {
      return -LO_EINVALIDBUND;  // not a bundle
    }
    pos += len;
    remain -= len;

    // time tag
    if (remain < 8) {
      return -LO_ESIZE;
    }
    pos += 8;
    remain -= 8;

    while (remain >= 4) {
      elem_len = lo_otoh32(*((uint32_t *)pos));
      pos += 4;
      remain -= 4;
      if (elem_len > remain) {
        return -LO_ESIZE;
      }
      pos += elem_len;
      remain -= elem_len;
    }
    if (0 != remain) {
      return -LO_ESIZE;
    }
    return size;
  }

  int parseOSC(void *data, uint64_t size) {
    int result = 0;
    char *path = (char *)data;
    ssize_t len = lo_validate_string(data, size);
    if (len < 0) {
      ESP_LOGI(LO_OSC_TAG, "Invalid message path. Length: %i", (int)(len));
      return len;
    }

    if (!strcmp((const char *)data, "#bundle")) {
      ESP_LOGI(LO_OSC_TAG, "Bundle recieved");
      char *pos;
      int remain;
      uint32_t elem_len;
      lo_timetag ts, now;

      ssize_t bundle_result = lo_validate_bundle(data, size);
      if (bundle_result < 0) {
        ESP_LOGI(LO_OSC_TAG, "Invalid bundle. Length: %i",
                 (int)(bundle_result));
        return bundle_result;
      }
      pos = (char *)data + len;
      remain = size - len;

      lo_timetag_now(&now);
      ts.sec = lo_otoh32(*((uint32_t *)pos));
      pos += 4;
      ts.frac = lo_otoh32(*((uint32_t *)pos));
      pos += 4;
      remain -= 8;

      // if (s->bundle_start_handler)
      //     s->bundle_start_handler(ts, s->bundle_handler_user_data);

      while (remain >= 4) {
        lo_message msg;
        elem_len = lo_otoh32(*((uint32_t *)pos));
        pos += 4;
        remain -= 4;

        if (!strcmp(pos, "#bundle")) {
          parseOSC(pos, elem_len);
        } else {
          msg = lo_message_deserialise(pos, elem_len, &result);
          if (!msg) {
            ESP_LOGI(LO_OSC_TAG, "Invalid message received. Length: %i",
                     (int)(result));
            return -result;
          }
          // set timetag from bundle
          // msg->ts = ts;

          // bump the reference count so that it isn't
          // automatically released
          lo_message_incref(msg);

          // test for immediate dispatch
          if ((ts.sec == LO_TT_IMMEDIATE.sec &&
               ts.frac == LO_TT_IMMEDIATE.frac) ||
              lo_timetag_diff(ts, now) <= 0.0) {
            callbackHandlerOSC(lo_get_path(pos, elem_len), msg);
            lo_message_free(msg);
          } else {
            // queue_data(s, ts, pos, msg, sock);
            callbackHandlerOSC(lo_get_path(pos, elem_len), msg);
            lo_message_free(msg);
          }
        }

        pos += elem_len;
        remain -= elem_len;
      }

      // if (s->bundle_end_handler)
      //     s->bundle_end_handler(s->bundle_handler_user_data);

    } else {
      lo_message msg = lo_message_deserialise(data, size, &result);
      if (NULL == msg) {
        ESP_LOGI(LO_OSC_TAG, "Invalid message received. Length: %i",
                 (int)(result));
        return -result;
      }
      lo_message_incref(msg);
      callbackHandlerOSC(lo_get_path(data, size), msg);
      lo_message_free(msg);
    }
    return size;
  }

  void tranfuncCallback(const char *path, const char *types, lo_arg **argv,
                        int argc, lo_message msg, void *user_data) {
    int32_t datasize = 0;
    int offset = 0;
    lo_arg *data = nullptr;

    for (int i = 0; i < argc; ++i) {
      lo_type type = (lo_type)types[i];
      switch (type) {
        case LO_INT32:
        case LO_FLOAT:
          offset = static_cast<int>(lo_hires_val(type, argv[i]));
          // printf("offset: %i \n", offset);
          break;
        case LO_STRING:
          printf("Recieved OSC string: %s at path: %s \n", &argv[i]->s, path);
          break;
        case LO_BLOB:
          datasize = lo_blob_datasize(argv[i]);
          data = argv[i];
          break;
        default:
          break;
      }
    }
    // cout << "looking up : " << path << endl;
    if (tfMap.find(path) != tfMap.end()) {
      TranFunc *tf = tfMap[path];
      // cout << "found " << path << " in tfMap" << endl;
      tf->updateFromGUI(datasize, offset, (float *)lo_blob_dataptr(data));
    }
  }

  void nentryCallback(const char *path, const char *types, lo_arg **argv,
                      int argc, lo_message msg, void *user_data) {
    static float num_val;

    for (int i = 0; i < argc; ++i) {
      lo_type type = (lo_type)types[i];
      switch (type) {
        case LO_INT32:
        case LO_FLOAT:
          num_val = lo_hires_val(type, argv[i]);
          setParamValue(path, num_val);
          ESP_LOGI(LO_OSC_TAG, "path %s , value %f \n", path,
                   static_cast<float>(num_val));
          break;
        case LO_STRING:
          printf("Recieved OSC string: %s at path: %s \n", &argv[i]->s, path);
          break;
        case LO_BLOB:
          printf("Recieved OSC blob: %s at path: %s \n", &argv[i]->s, path);
          break;
        default:
          break;
      }
    }
  }

  void callbackHandlerOSC(char *path, lo_message msg) {
    int argc = lo_message_get_argc(msg);
    lo_arg **argv = lo_message_get_argv(msg);
    char *types = lo_message_get_types(msg);

    // Callback for standard nentry and sliders.
    nentryCallback(path, types, argv, argc, msg, NULL);

    // Callback for table-based widgets. TransferFunction_free, ...

    // 		if (string(path).find("/TransferFunction_free") != string::npos)
    // { 			tranfuncCallback(msg, path);
  }

  // Recieve websocket binary messages.
  // This function expects an OSC formated message
  void BinMsgRecievedCallback(char *data, uint64_t len) {
    binMsg_t msg;
    msg.data = static_cast<char *>(malloc(len));
    // xSemaphoreTake(msq_queue_mutex, portMAX_DELAY);
    memcpy(msg.data, data, len);
    msg.len = len;
    msgQueue.push(msg);
    // xSemaphoreGive(msq_queue_mutex);
  }

  void handleOscMsgQueue() {
    // xSemaphoreTake(msq_queue_mutex, portMAX_DELAY);
    if (msgQueue.size() > 50) {
      printf("MsgQue size:  %i \n", msgQueue.size());
    }
    while (!msgQueue.empty()) {
      binMsg_t msg = msgQueue.front();
      parseOSC(msg.data, msg.len);
      msgQueue.pop();
    }
    // xSemaphoreGive(msq_queue_mutex);
  }

  // Recieve websocket text message.
  void TextMsgRecievedCallback(char *data, uint64_t len) {
    msg_item_t msg;
    if (unpack(data, &msg)) {
      setParamValue(msg.path, msg.value);
      cout << "Path" << msg.path << "Value" << msg.value << endl;
    } else {
      ESP_LOGI(TEXT_MSG_RECIEVED_TAG,
               "Error in recieved websocket text message ");
    }
  }

  void update() {
    char msg[ws->msgMaxSize];
    const static char *TAG = "Gui_update_task";
    ESP_LOGI(TAG, "task starting");

    while (true) {
      // Read from external guis
      handleOscMsgQueue();

      vTaskDelay(GUI_UPDATE_MS / portTICK_PERIOD_MS);

      // TODO: CHECK IF CHANGED!
      // Write to external guis
      updateWSGui(msg);
    }
  }

  // Empty url passed as the "*to" argument in lo_message_serialize,
  // since we are sending to websocket clients and not via udp.
  // TODO: Send to specific clients by assigning a client number in this arg

  void updateWSGui(char *msg) {
    static char url[1] = "";
    size_t size = 0;
    for (auto &item : fPathZoneMap) {
      if (!MetaDataUI::isHidden(
              item.second)) {  // Only send data for non hidden widgets
        const char *path = static_cast<const char *>(item.first.c_str());
        lo_message oscMsg = lo_message_new();
        if (0 == lo_message_add_float(oscMsg, *item.second)) {
          static_cast<char *>(lo_message_serialise(oscMsg, path, msg, &size));
          ws_server_send_bin_all(msg, static_cast<uint64_t>(size));
        }
        lo_message_free(oscMsg);
      }
    }
  }

  void TextSendCallback(char *msg) {
    // Update all guis
    for (auto &item : fPathZoneMap) {
      uint64_t len = pack(msg, item.first, *item.second);
      ws_server_send_text_all(msg, len);
    }
  }

  static void updateTranFuncHandler(void *arg) {
    static_cast<Esp32WebsocketUI *>(arg)->updateTranFunc();
  }
  static void updateHandler(void *arg) {
    static_cast<Esp32WebsocketUI *>(arg)->update();
  }

  static void TextMsgRecievedCallbackHandler(void *arg, char *data,
                                             uint64_t len) {
    static_cast<Esp32WebsocketUI *>(arg)->TextMsgRecievedCallback(data, len);
  }

  static void BinMsgRecievedCallbackHandler(void *arg, char *data,
                                            uint64_t len) {
    static_cast<Esp32WebsocketUI *>(arg)->BinMsgRecievedCallback(data, len);
  }
};
#endif
