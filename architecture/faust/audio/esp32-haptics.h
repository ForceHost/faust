/************************** BEGIN esp32audio.h **************************/
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2020 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.

 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __esp32audio__
#define __esp32audio__

#include <utility>
#include <vector>

#include "driver/gpio.h"
#include "driver/i2s.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_timer.h"
#include "faust/audio/audio.h"
#include "faust/gui/Esp32MemoryManager.h"
#include "faust/haptic/Esp32HapticController.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

//////// Constants //////////

#define MULT_S32 2147483647
#define DIV_S32 4.6566129e-10
#define clip(sample) std::max(-MULT_S32, std::min(MULT_S32, ((int32_t)(sample * MULT_S32))));

//////// Settings //////////

//#define HAPTIC_AUDIO_RATE  // for fs > 1KHz or SYNTH_A_MODELLER
// #define SYNCHRONOUS_HAPTICS 
// #define USE_GPIO_DEBUG
#define MAX_CHAN 2
#define AUDIO_TASK_PRIORITY 25

gpio_num_t GPIO_DEBUG_0 = GPIO_NUM_13;
gpio_num_t GPIO_DEBUG_1 = GPIO_NUM_15;
////////////////////////////

#ifdef HAPTIC_AUDIO_RATE
const int NUM_HAPTIC_DOF_IN_AUDIO = NUM_HAPTIC_DOF;
#else
const int NUM_HAPTIC_DOF_IN_AUDIO = 0;
#endif

class esp32audio : public audio {
   private:
    unsigned int                fSampleRate;
    int                fBufferSize;
    int64_t            fMaxRenderTime;
    float              fAvgLoad = 0;
    int                fNumInputs;
    int                fNumOutputs;
    float**            fInChannel;
    float**            fOutChannel;
    TaskHandle_t       fHandle;
    dsp*               fDSP;
    bool               fRunning;
    HapticController*  fHaptics;
    Esp32MutexManager* fMemGuard;

    template <int AUDIO_INPUTS, int AUDIO_OUTPUTS, int NUM_HAPTIC_INPUTS, int NUM_HAPTIC_OUTPUTS>
    void audioTask() {
        const static char* TAG        = "esp32audio::audioTask()";
        int64_t            start      = 0;
        int64_t            time_spent = 0;
        ESP_LOGI(TAG,
                 "started with %i audio inputs and %i audio outputs, %i "
                 "haptic inputs and %i haptic outputs\n",
                 AUDIO_INPUTS, AUDIO_OUTPUTS, NUM_HAPTIC_INPUTS, NUM_HAPTIC_OUTPUTS);
        while (fRunning) {
#ifdef USE_GPIO_DEBUG
            gpio_set_level(GPIO_DEBUG_0, 1);
#endif
#ifdef SYNCHRONOUS_HAPTICS
            start = esp_timer_get_time();
            // Update inputs of haptic devices
            fHaptics->update_inputs();
            time_spent += esp_timer_get_time() - start;
#endif
            for (int j = 0; j < NUM_HAPTIC_INPUTS; ++j) {
                for (int i = 0; i < fBufferSize; ++i) {
                    fInChannel[j][i] = 0.5;
                    if (fHaptics->devices.size() >= NUM_HAPTIC_INPUTS) {
                        fHaptics->devices[j]->get_position();
                    }
                }
            }

#ifdef USE_GPIO_DEBUG
            gpio_set_level(GPIO_DEBUG_0, 0);
#endif
            if (AUDIO_INPUTS > 0) {
                // Read from the card
                int32_t samples_data_in[(MAX_CHAN)*fBufferSize];
                size_t  bytes_read = 0;
                i2s_read((i2s_port_t)0, samples_data_in, MAX_CHAN * sizeof(float) * fBufferSize, &bytes_read,
                         portMAX_DELAY);

                // Convert and copy inputs
                if (AUDIO_INPUTS == MAX_CHAN) {
                    // if stereo
                    for (int i = 0; i < fBufferSize; i++) {
                        fInChannel[NUM_HAPTIC_INPUTS][i]     = (float)samples_data_in[i * MAX_CHAN] * DIV_S32;
                        fInChannel[NUM_HAPTIC_INPUTS + 1][i] = (float)samples_data_in[i * MAX_CHAN + 1] * DIV_S32;
                    }
                } else {
                    // otherwise only first channel
                    for (int i = 0; i < fBufferSize; i++) {
                        fInChannel[NUM_HAPTIC_INPUTS][i] = (float)samples_data_in[i * MAX_CHAN] * DIV_S32;
                    }
                }
            }
            // Get current time
            start = esp_timer_get_time();
            // take semmaphore
            fMemGuard->lock_resource();
            // Call DSP
            fDSP->compute(fBufferSize, fInChannel, fOutChannel);

            // give semaphore
            fMemGuard->free_resource();

            time_spent += esp_timer_get_time() - start;
            if (AUDIO_OUTPUTS > 0) {
                // Convert and copy outputs
                int32_t samples_data_out[MAX_CHAN * fBufferSize];
                if (AUDIO_OUTPUTS == MAX_CHAN) {
                    // if stereoz
                    for (int i = 0; i < fBufferSize; i++) {
                        samples_data_out[i * MAX_CHAN]     = clip(fOutChannel[NUM_HAPTIC_OUTPUTS][i]);
                        samples_data_out[i * MAX_CHAN + 1] = clip(fOutChannel[NUM_HAPTIC_OUTPUTS + 1][i]);
                    }
                } else {
                    // otherwise only first channel
                    for (int i = 0; i < fBufferSize; i++) {
                        samples_data_out[i * MAX_CHAN]     = clip(fOutChannel[NUM_HAPTIC_OUTPUTS][i]);
                        samples_data_out[i * MAX_CHAN + 1] = samples_data_out[i * MAX_CHAN];
                    }
                }

                // Write to the card

                size_t bytes_written = 0;
                i2s_write((i2s_port_t)0, &samples_data_out, MAX_CHAN * sizeof(float) * fBufferSize, &bytes_written,
                          portMAX_DELAY);

            } else {
                vTaskDelay(1 / portTICK_PERIOD_MS);
            }

#ifdef USE_GPIO_DEBUG
            gpio_set_level(GPIO_DEBUG_1, 1);
#endif
#ifdef SYNCHRONOUS_HAPTICS  // Synchronous update of haptic devices (blocking)
            start = esp_timer_get_time();
            fHaptics->update_outputs();
            time_spent += esp_timer_get_time() - start;
#else  // Asynchronous update of haptic devices (non-blocking)
            xTaskNotifyGive(fHaptics->fHandle);
#endif

            // Write most recent haptic sample to device
            for (int i = 0; i < NUM_HAPTIC_OUTPUTS; ++i) {
                if (fHaptics->devices.size() >= NUM_HAPTIC_OUTPUTS) {
                    fHaptics->devices[i]->set_force(fOutChannel[i][0]);
                }
            }

            logCPULoad(time_spent);
            time_spent = 0;
#ifdef USE_GPIO_DEBUG
            gpio_set_level(GPIO_DEBUG_1, 0);
#endif
        }

        // Task has to delete itself beforee returning
        vTaskDelete(nullptr);
    }

    void destroy() {
        for (int i = 0; i < fNumInputs; i++) {
            delete[] fInChannel[i];
        }
        delete[] fInChannel;

        for (int i = 0; i < fNumOutputs; i++) {
            delete[] fOutChannel[i];
        }
        delete[] fOutChannel;
    }

    static void audioTaskHandler(void* arg) {
        const static char* TAG   = "Audio task handler";
        esp32audio*        audio = static_cast<esp32audio*>(arg);
        ESP_LOGI(TAG, "Total number of Inputs: %i, Total number of Outputs: %i  \n", audio->fNumInputs,
                 audio->fNumOutputs);

        int numInputs  = audio->fNumInputs - NUM_HAPTIC_DOF_IN_AUDIO;
        int numOutputs = audio->fNumOutputs - NUM_HAPTIC_DOF_IN_AUDIO;

        // Handle mismatch between number of faust I/O's and haptic dofs (support up
        // to 2 dof)
        if (numInputs < 0) {
            ESP_LOGI(TAG, "The applications has %i inputs, but the haptic device requires %i", audio->fNumInputs,
                     NUM_HAPTIC_DOF_IN_AUDIO);
        }

        if (numInputs > 2) {
            numInputs = 2;
            ESP_LOGI(TAG,
                     "Multichannel applications not supported. Number of inputs "
                     "clipped at 2");
        }

        if (numOutputs < 0) {
            ESP_LOGI(TAG, "The applications has %i outputs, but the haptic device requires %i", audio->fNumOutputs,
                     NUM_HAPTIC_DOF_IN_AUDIO);
        }

        if (numOutputs > 2) {
            numOutputs = 2;
            ESP_LOGI(TAG,
                     "Multichannel applications not supported. Number of outputs "
                     "clipped at 2");
        }

        // Start audio task
        if (numInputs == -2 && numOutputs == -2) {
            audio->audioTask<0, 0, NUM_HAPTIC_DOF_IN_AUDIO - 2, NUM_HAPTIC_DOF_IN_AUDIO - 2>();
        } else if (numInputs == -2 && numOutputs == -1) {
            audio->audioTask<0, 0, NUM_HAPTIC_DOF_IN_AUDIO - 2, NUM_HAPTIC_DOF_IN_AUDIO - 1>();
        } else if (numInputs == -2 && numOutputs == 0) {
            audio->audioTask<0, 0, NUM_HAPTIC_DOF_IN_AUDIO - 2, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else if (numInputs == -1 && numOutputs == 0) {
            audio->audioTask<0, 0, NUM_HAPTIC_DOF_IN_AUDIO - 1, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else if (numInputs == 0 && numOutputs == 0) {
            audio->audioTask<0, 0, NUM_HAPTIC_DOF_IN_AUDIO, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else if (numInputs == 0 && numOutputs == 1) {
            audio->audioTask<0, 1, NUM_HAPTIC_DOF_IN_AUDIO, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else if (numInputs == 0 && numOutputs == 2) {
            audio->audioTask<0, 2, NUM_HAPTIC_DOF_IN_AUDIO, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else if (numInputs == 1 && numOutputs == 1) {
            audio->audioTask<1, 1, NUM_HAPTIC_DOF_IN_AUDIO, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else if (numInputs == 1 && numOutputs == 2) {
            audio->audioTask<1, 2, NUM_HAPTIC_DOF_IN_AUDIO, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else if (numInputs == 2 && numOutputs == 1) {
            audio->audioTask<2, 1, NUM_HAPTIC_DOF_IN_AUDIO, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else if (numInputs == 2 && numOutputs == 2) {
            audio->audioTask<2, 2, NUM_HAPTIC_DOF_IN_AUDIO, NUM_HAPTIC_DOF_IN_AUDIO>();
        } else {
            audio->audioTask<2, 2, NUM_HAPTIC_DOF_IN_AUDIO, NUM_HAPTIC_DOF_IN_AUDIO>();
            printf(
                "Multichannel applications not supported. Audio task started with 2 "
                "Inputs and 2 Outputs\n");
        }
    }

   public:
    esp32audio(int srate, int bsize)
        : fSampleRate(srate),
          fBufferSize(bsize),
          fNumInputs(0),
          fNumOutputs(0),
          fInChannel(nullptr),
          fOutChannel(nullptr),
          fHandle(nullptr),
          fRunning(false) {
        i2s_pin_config_t pin_config;
#if TTGO_TAUDIO
        pin_config = {.bck_io_num = 33, .ws_io_num = 25, .data_out_num = 26, .data_in_num = 27};
#elif A1S_BOARD
        pin_config              = {.bck_io_num = 27, .ws_io_num = 26, .data_out_num = 25, .data_in_num = 35};
#elif LYRA_V4_3
        pin_config              = {.mck_io_num = -1, .bck_io_num = 5, .ws_io_num = 25, .data_out_num = 26, .data_in_num = 35};
#elif TSTICK_JOINT
        pin_config = {.bck_io_num = 5, .ws_io_num = 25, .data_out_num = 26, .data_in_num = 35};
#else  // Default
        pin_config = {.bck_io_num = 33, .ws_io_num = 25, .data_out_num = 26, .data_in_num = 27};
#endif

#if A1S_BOARD
        i2s_config_t i2s_config = {
            .mode                 = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_RX),
            .sample_rate          = fSampleRate,
            .bits_per_sample      = I2S_BITS_PER_SAMPLE_32BIT,
            .channel_format       = I2S_CHANNEL_FMT_RIGHT_LEFT,
            .communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_STAND_I2S),
            .intr_alloc_flags     = ESP_INTR_FLAG_LEVEL3,  // high interrupt priority
            .dma_buf_count        = 3,
            .dma_buf_len          = fBufferSize,
            .use_apll             = true};
#elif LYRA_V4_3  // Check this!
        i2s_config_t i2s_config = {
            .mode                 = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_RX),
            .sample_rate          = fSampleRate,
            .bits_per_sample      = I2S_BITS_PER_SAMPLE_32BIT,
            .channel_format       = I2S_CHANNEL_FMT_RIGHT_LEFT,
            .communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_STAND_I2S),
            .intr_alloc_flags     = ESP_INTR_FLAG_LEVEL1,  // high interrupt priority
            // .intr_alloc_flags = ESP_INTR_FLAG_NMI,  // high interrupt priority
            .dma_buf_count      = 8, // a high value is needed otherwise audio is interrupted by clicks
            .dma_buf_len        = fBufferSize,
            .use_apll           = true,
            .tx_desc_auto_clear = true,
            .fixed_mclk = 0,
            .mclk_multiple = I2S_MCLK_MULTIPLE_DEFAULT, 
            .bits_per_chan = I2S_BITS_PER_CHAN_DEFAULT,
        };
#else            // default
        i2s_config_t i2s_config = {
            .mode                 = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_RX),
            .sample_rate          = fSampleRate,
            .bits_per_sample      = I2S_BITS_PER_SAMPLE_32BIT,
            .channel_format       = I2S_CHANNEL_FMT_RIGHT_LEFT,
            .communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_STAND_I2S),
            .intr_alloc_flags     = ESP_INTR_FLAG_LEVEL1,  // high interrupt priority
            .dma_buf_count        = 3,
            .dma_buf_len          = fBufferSize,
            .use_apll             = false};
#endif
        i2s_driver_install((i2s_port_t)0, &i2s_config, 0, nullptr);
        i2s_set_pin((i2s_port_t)0, &pin_config);
        PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0_CLK_OUT1);
        REG_WRITE(PIN_CTRL, 0xFFFFFFF0);
        fMemGuard = new Esp32MutexManager();
    }

    virtual ~esp32audio() { destroy(); }

    virtual bool init(const char* name, dsp* dsp) {
        destroy();

        fDSP           = dsp;
        fNumInputs     = fDSP->getNumInputs();
        fNumOutputs    = fDSP->getNumOutputs();
        fMaxRenderTime = (int)(float(fBufferSize) / fSampleRate * 1000000 + 0.5);
#ifdef USE_GPIO_DEBUG
        gpio_pad_select_gpio(GPIO_DEBUG_0);
        gpio_set_direction(GPIO_DEBUG_0, GPIO_MODE_OUTPUT);
        gpio_pad_select_gpio(GPIO_DEBUG_1);
        gpio_set_direction(GPIO_DEBUG_1, GPIO_MODE_OUTPUT);
#endif

        fDSP->init(fSampleRate);

        if (fNumInputs > 0) {
            fInChannel = new FAUSTFLOAT*[fNumInputs];
            for (int i = 0; i < fNumInputs; i++) {
                fInChannel[i] = new FAUSTFLOAT[fBufferSize];
            }
        } else {
            fInChannel = nullptr;
        }

        if (fNumOutputs > 0) {
            fOutChannel = new FAUSTFLOAT*[fNumOutputs];
            for (int i = 0; i < fNumOutputs; i++) {
                fOutChannel[i] = new FAUSTFLOAT[fBufferSize];
            }
        } else {
            fOutChannel = nullptr;
        }

        return true;
    }

    void add_shared_owner(Esp32UiMutex* ui_mutex) { fMemGuard->add_owner(ui_mutex); }

    void initHaptics(HapticController* fHaptics_) { fHaptics = fHaptics_; }

    virtual bool start() {
        if (!fRunning) {
            fRunning = true;
            if ((xTaskCreatePinnedToCore(audioTaskHandler, "Faust DSP Task", 16384, (void*)this, AUDIO_TASK_PRIORITY,
                                         &fHandle, 1) == pdPASS)) {
#ifdef SYNCHRONOUS_HAPTICS  // Haptic devices are updated from within audio loop
                return true;
#else  // Haptic devices are updated asynchronously in a seperate task, running at 1 Khz
                return fHaptics->run();
#endif
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    virtual void stop() {
        if (fRunning) {
            fRunning = false;
            vTaskDelay(1 / portTICK_PERIOD_MS);
            fHandle = nullptr;
        }
    }

    virtual int getBufferSize() { return fBufferSize; }
    virtual int getSampleRate() { return fSampleRate; }

    virtual int getNumInputs() { return MAX_CHAN; }
    virtual int getNumOutputs() { return MAX_CHAN; }

    virtual float getHapticInput() { return fInChannel[0][0]; }
    virtual float getHapticOutput() { return fOutChannel[0][0]; }

    // Returns the average proportion of available CPU being spent inside the
    // audio callbacks (between 0 and 1.0).
    virtual float getCPULoad() { return fAvgLoad; }

    float logCPULoad(int64_t dt) {
        static int64_t sum_dt;
        static int     count;
        sum_dt += dt;
        count++;
        if (count > 500) {
            fAvgLoad = float(sum_dt) / (count * fMaxRenderTime);
            // fAvgLoad = float(sum_dt) / (count);
            std::cout << " DSP performance : " << fAvgLoad * 100 << "%" << std::endl;

            count  = 0;
            sum_dt = 0;
        }
        return fAvgLoad;
    }
};

#endif
/**************************  END  esp32fHaptics.h **************************/
