/*
        ES8388 - a ES8388 Codec driver library for ESP-IDF
        ported to esp-idf by Mathias Kirkegaard

        Copyright (C) 2020, Mathias Kirkegaard, IDMIL

        This is boilerplate code that integrates the driver in:
        https://github.com/espressif/esp-adf/tree/master/components/audio_hal/driver/es8388

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ES8388.h"

#include <iostream>

static const char* ES_TAG = "ES8388_DRIVER";

void ES8388::readParams(int FS, int BIT_DEPTH) {
    // MCLK = 12.288 MHz
    // if(FS == 8000){
    // 	div = 0x0A;
    // } else if(FS == 12000){
    // 	div = 0x07;
    // } else if(FS == 16000){
    // 	div = 0x06;
    // } else if(FS == 24000){
    // 	div = 0x04;
    // } else if(FS == 32000){
    // 	div = 0x03;
    // } else if(FS == 48000){
    // 	div = 0x02;
    // } else if (FS == 96000){
    // 	div = 0x00;
    // }

    // MCLK = 18.432 MHz
    if (FS == 8000) {
        div = 0x0C;  // 01100
    } else if (FS == 12000) {
        div = 0x0A;  // 01010
    } else if (FS == 16000) {
        div = 0x08;  // 01000
    } else if (FS == 24000) {
        div = 0x06;  // 00110
    } else if (FS == 32000) {
        div = 0x05;  // 00101
    } else if (FS == 48000) {
        div = 0x03;  // 00011
    } else if (FS == 96000) {
        div = 0x01;  // 00001
    }

    if (BIT_DEPTH == 16) {
        bits = BIT_LENGTH_16BITS;
    } else if (BIT_DEPTH == 18) {
        bits = BIT_LENGTH_18BITS;
    } else if (BIT_DEPTH == 20) {
        bits = BIT_LENGTH_20BITS;
    } else if (BIT_DEPTH == 24) {
        bits = BIT_LENGTH_24BITS;
    } else if (BIT_DEPTH == 32) {
        bits = BIT_LENGTH_32BITS;
    }
}

esp_err_t ES8388::Init(int FS, int BIT_DEPTH) {
    esp_err_t res = ESP_OK;
    readParams(FS, BIT_DEPTH);
    res = InitI2C();  // ESP32 in master mode
    vTaskDelay(500 / portTICK_PERIOD_MS);

    res |= WriteReg(ES8388_DACCONTROL3,
                    0x04);  // 0x04 mute/0x00 unmute&ramp;DAC unmute and  disabled
                            // digital volume control soft ramp
    /* Chip Control and Power Management */
    res |= WriteReg(ES8388_CONTROL2, 0x50);
    res |= WriteReg(ES8388_CHIPPOWER, 0x00);  // normal all and power up all
    res |= WriteReg(ES8388_MASTERMODE,
                    AUDIO_HAL_MODE_SLAVE);  // CODEC IN I2S SLAVE MODE

    /* dac */
    res |= WriteReg(ES8388_DACPOWER, 0xC0);  // disable DAC and disable Lout/Rout/1/2
    res |= WriteReg(ES8388_CONTROL1,
                    0x12);  // Enfr=0,Play&Record Mode,(0x17-both of mic&paly)
    //    res |= WriteReg(ES8388_CONTROL2, 0);  //LPVrefBuf=0,Pdn_ana=0
    res |= WriteReg(ES8388_DACCONTROL1, 0x18);  // 1a 0x18:16bit iis , 0x00:24
    res |= WriteReg(ES8388_DACCONTROL2,
                    0x02);  // DACFsMode,SINGLE SPEED; DACFsRatio,256
    res |= WriteReg(ES8388_DACCONTROL16,
                    0x00);  // 0x00 audio on LIN1&RIN1,  0x09 LIN2&RIN2
    res |= WriteReg(ES8388_DACCONTROL17,
                    0x90);  // only left DAC to left mixer enable 0db
    res |= WriteReg(ES8388_DACCONTROL20,
                    0x90);  // only right DAC to right mixer enable 0db
    res |= WriteReg(ES8388_DACCONTROL21,
                    0x80);                        // set internal ADC and DAC use the same LRCK clock,
                                                  // ADC LRCK as internal LRCK
    res |= WriteReg(ES8388_DACCONTROL23, 0x00);   // vroi=0
    res |= SetAdcDacVolume(ES_MODULE_DAC, 0, 0);  // 0db

    int tmp = 0;
    // if (AUDIO_HAL_DAC_OUTPUT_LINE2 == cfg->dac_output) {
    // 	tmp = DAC_OUTPUT_LOUT1 | DAC_OUTPUT_ROUT1;
    // } else if (AUDIO_HAL_DAC_OUTPUT_LINE1 == cfg->dac_output) {
    // 	tmp = DAC_OUTPUT_LOUT2 | DAC_OUTPUT_ROUT2;
    // } else {
    // 	tmp = DAC_OUTPUT_LOUT1 | DAC_OUTPUT_LOUT2 | DAC_OUTPUT_ROUT1 |
    // DAC_OUTPUT_ROUT2;
    // }

    tmp = DAC_OUTPUT_LOUT1 | DAC_OUTPUT_LOUT2 | DAC_OUTPUT_ROUT1 | DAC_OUTPUT_ROUT2;
    res |= WriteReg(ES8388_DACPOWER,
                    tmp);  // 0x3c Enable DAC and Enable Lout/Rout/1/2

    /* adc */
    res |= WriteReg(ES8388_ADCPOWER, 0xFF);
    // res |= WriteReg(ES8388_ADCCONTROL1,
    //               0xbb);  // MIC Left and Right channel PGA gain

    tmp = 0;
    // if (AUDIO_HAL_ADC_INPUT_LINE1 == cfg->adc_input) {
    // 	tmp = ADC_INPUT_LINPUT1_RINPUT1;
    // } else if (AUDIO_HAL_ADC_INPUT_LINE2 == cfg->adc_input) {
    // 	tmp = ADC_INPUT_LINPUT2_RINPUT2;
    // } else {
    // 	tmp = ADC_INPUT_DIFFERENCE;
    // }

    tmp = ADC_INPUT_LINPUT2_RINPUT2;
    res |= WriteReg(ES8388_ADCCONTROL2,
                    tmp);  // 0x00 LINSEL & RINSEL, LIN1/RIN1 as ADC Input;
                           // DSSEL,use one DS Reg11; DSR, LINPUT1-RINPUT1

    // res |= WriteReg(ES8388_ADCCONTROL3, 0x02);
    // res |= WriteReg(ES8388_ADCCONTROL4,
    //                0x0d);                      // Left/Right data, Left/Right justified mode, Bits
    // length, I2S format
    res |= WriteReg(ES8388_ADCCONTROL5, 0x02);  // ADCFsMode,singel
                                                // SPEED,RATIO=256
    // ALC for Microphone
    res |= SetAdcDacVolume(ES_MODULE_ADC, 0, 0);  // 0db
    // res |= WriteReg(ES8388_ADCPOWER,
    //               0x09);  // Power on ADC, Enable LIN&RIN, Power off MICBIAS,
    // set int1lp to low power mode

    /* disable es8388 PA */
    PaPower(false);

    // Set voice volume
    SetVoiceVolume(100);

    // Start the audio codec
    res |= Start(mode);

    // Delete i2c driver
    res |= i2c_driver_delete((i2c_port_t)I2C_MASTER_NUM);

    return res;
}

esp_err_t ES8388::InitI2C() {
    i2c_port_t   i2c_master_port = (i2c_port_t)I2C_MASTER_NUM;
    i2c_config_t i2c_config;
    memset(&i2c_config, 0, sizeof(i2c_config));
    i2c_config.mode             = I2C_MODE_MASTER;
    i2c_config.sda_io_num       = (gpio_num_t)I2C_MASTER_SDA_IO;
    i2c_config.sda_pullup_en    = GPIO_PULLUP_ENABLE;
    i2c_config.scl_io_num       = (gpio_num_t)I2C_MASTER_SCL_IO;
    i2c_config.scl_pullup_en    = GPIO_PULLUP_ENABLE;
    i2c_config.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_config.clk_flags = 0;
    ESP_ERROR_CHECK(i2c_param_config(i2c_master_port, &i2c_config));
    i2c_set_timeout(i2c_master_port,
                    I2C_MASTER_TIMEOUT);  // Has to be after param_config
    return i2c_driver_install(i2c_master_port, i2c_config.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE,
                              0);
}

esp_err_t ES8388::WriteReg(uint8_t reg, uint8_t val) {
    esp_err_t ret = ESP_OK;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ret |= i2c_master_start(cmd);
    ret |= i2c_master_write_byte(cmd, (ES8388_ADDR << 1) | WRITE_BIT, (i2c_ack_type_t)ACK_CHECK_EN);
    ret |= i2c_master_write_byte(cmd, reg, (i2c_ack_type_t)ACK_CHECK_EN);
    ret |= i2c_master_write_byte(cmd, val, (i2c_ack_type_t)ACK_CHECK_EN);
    ret |= i2c_master_stop(cmd);
    ret |= i2c_master_cmd_begin((i2c_port_t)I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    return ret;
}

esp_err_t ES8388::ReadReg(uint8_t reg_add, uint8_t* val) {
    esp_err_t        ret = ESP_OK;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ret |= i2c_master_start(cmd);
    ret |= i2c_master_write_byte(cmd, (ES8388_ADDR << 1) | WRITE_BIT, (i2c_ack_type_t)ACK_CHECK_EN);
    ret |= i2c_master_write_byte(cmd, reg_add, (i2c_ack_type_t)ACK_CHECK_EN);
    ret |= i2c_master_stop(cmd);
    ret |= i2c_master_cmd_begin((i2c_port_t)I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    cmd = i2c_cmd_link_create();
    ret |= i2c_master_start(cmd);
    ret |= i2c_master_write_byte(cmd, (ES8388_ADDR << 1) | READ_BIT, (i2c_ack_type_t)ACK_CHECK_EN);
    ret |= i2c_master_read_byte(cmd, val, (i2c_ack_type_t)NACK_VAL);
    ret |= i2c_master_stop(cmd);
    ret |= i2c_master_cmd_begin((i2c_port_t)I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    return ret;
}

void ES8388::ReadAll() {
    for (int i = 0; i < 50; i++) {
        uint8_t reg = 0;
        ReadReg(i, &reg);
        printf("%x: %x\n", i, reg);
    }
}

esp_err_t ES8388::SetAdcDacVolume(int mode, int volume, int dot) {
    int res = 0;
    if (volume < -96 || volume > 0) {
        ESP_LOGW(ES_TAG, "Warning: volume < -96! or > 0!\n");
        if (volume < -96)
            volume = -96;
        else
            volume = 0;
    }
    dot    = (dot >= 5 ? 1 : 0);
    volume = (-volume << 1) + dot;
    if (mode == ES_MODULE_ADC || mode == ES_MODULE_ADC_DAC) {
        res |= WriteReg(ES8388_ADCCONTROL8, volume);
        res |= WriteReg(ES8388_ADCCONTROL9, volume);  // ADC Right Volume=0db
    }
    if (mode == ES_MODULE_DAC || mode == ES_MODULE_ADC_DAC) {
        res |= WriteReg(ES8388_DACCONTROL5, volume);
        res |= WriteReg(ES8388_DACCONTROL4, volume);
    }
    return res;
}

esp_err_t ES8388::SetI2SWordSize(es_module_t mode, es_bits_length_t bit_per_sample) {
    esp_err_t res  = ESP_OK;
    uint8_t   reg  = 0;
    int       bits = (int)bit_per_sample;

    if (mode == ES_MODULE_ADC || mode == ES_MODULE_ADC_DAC) {
        res = ReadReg(ES8388_ADCCONTROL4, &reg);
        reg = reg & 0xe3;
        res |= WriteReg(ES8388_ADCCONTROL4, reg | (bits << 2));
    }
    if (mode == ES_MODULE_DAC || mode == ES_MODULE_ADC_DAC) {
        res = ReadReg(ES8388_DACCONTROL1, &reg);
        reg = reg & 0xc7;
        res |= WriteReg(ES8388_DACCONTROL1, reg | (bits << 3));
    }
    return res;
}

esp_err_t ES8388::SetMclkDiv(int mclkDiv) {
    esp_err_t res = ESP_OK;
    res |= WriteReg(ES8388_DACCONTROL2, mclkDiv);  // set Samplerate
    res |= WriteReg(ES8388_ADCCONTROL5, mclkDiv);
    return res;
}

esp_err_t ES8388::SetI2sFormat(es_module_t mod, es_i2s_fmt_t fmt) {
    esp_err_t res = ESP_OK;
    uint8_t   reg = 0;
    if (mode == ES_MODULE_ADC || mode == ES_MODULE_ADC_DAC) {
        res = ReadReg(ES8388_ADCCONTROL4, &reg);
        reg = reg & 0xfc;
        res |= WriteReg(ES8388_ADCCONTROL4, reg | fmt);
    }
    if (mode == ES_MODULE_DAC || mode == ES_MODULE_ADC_DAC) {
        res = ReadReg(ES8388_DACCONTROL1, &reg);
        reg = reg & 0xf9;
        res |= WriteReg(ES8388_DACCONTROL1, reg | (fmt << 1));
    }
    return res;
}

esp_err_t ES8388::I2SConfig() {
    esp_err_t res = ESP_OK;
    res |= SetI2sFormat(ES_MODULE_ADC_DAC, fmt);
    res |= SetI2SWordSize(ES_MODULE_ADC_DAC, bits);
    res |= SetMclkDiv(div);
    return res;
}

esp_err_t ES8388::Start(es_module_t mode) {
    esp_err_t res       = ESP_OK;
    uint8_t   prev_data = 0, data = 0;
    res |= I2SConfig();
    ReadReg(ES8388_DACCONTROL21, &prev_data);
    if (mode == ES_MODULE_LINE) {
        res |= WriteReg(ES8388_DACCONTROL16,
                        0x09);  // 0x00 audio on LIN1&RIN1,  0x09 LIN2&RIN2 by pass enable
        res |= WriteReg(ES8388_DACCONTROL17,
                        0x50);  // left DAC to left mixer enable  and  LIN signal to
                                // left mixer enable 0db  : bupass enable
        res |= WriteReg(ES8388_DACCONTROL20,
                        0x50);                       // right DAC to right mixer enable  and  LIN signal
                                                     // to right mixer enable 0db : bupass enable
        res |= WriteReg(ES8388_DACCONTROL21, 0xC0);  // enable adc
    } else {
        res |= WriteReg(ES8388_DACCONTROL21, 0x80);  // enable dac
    }
    ReadReg(ES8388_DACCONTROL21, &data);
    if (prev_data != data) {
        res |= WriteReg(ES8388_CHIPPOWER, 0xF0);  // start state machine
        // res |= WriteReg(ES8388_CONTROL1, 0x16);
        // res |= WriteReg(ES8388_CONTROL2, 0x50);
        res |= WriteReg(ES8388_CHIPPOWER, 0x00);  // start state machine
    }
    if (mode == ES_MODULE_ADC || mode == ES_MODULE_ADC_DAC || mode == ES_MODULE_LINE) {
        res |= WriteReg(ES8388_ADCPOWER, 0x00);  // power up adc and line in
    }
    if (mode == ES_MODULE_DAC || mode == ES_MODULE_ADC_DAC || mode == ES_MODULE_LINE) {
        res |= WriteReg(ES8388_DACPOWER, 0x3c);  // power up dac and line out
        res |= SetVoiceMute(false);
        ESP_LOGD(ES_TAG, "es8388_start default is mode:%d", mode);
    }

    return res;
}

/**
 * @param volume: 0 ~ 100
 *
 * @return
 *     - (-1)  Error
 *     - (0)   Success
 */
esp_err_t ES8388::SetVoiceVolume(int volume) {
    esp_err_t res = ESP_OK;
    if (volume < 0)
        volume = 0;
    else if (volume > 100)
        volume = 100;
    volume /= 3;
    res = WriteReg(ES8388_DACCONTROL24, volume);
    res |= WriteReg(ES8388_DACCONTROL25, volume);
    res |= WriteReg(ES8388_DACCONTROL26, 0);
    res |= WriteReg(ES8388_DACCONTROL27, 0);
    return res;
}

esp_err_t ES8388::SetVoiceMute(bool enable) {
    esp_err_t res = ESP_OK;
    uint8_t   reg = 0;
    res           = ReadReg(ES8388_DACCONTROL3, &reg);
    reg           = reg & 0xFB;
    res |= WriteReg(ES8388_DACCONTROL3, reg | (((int)enable) << 2));
    return res;
}

void ES8388::PaPower(bool enable) {
    gpio_config_t io_conf;
    memset(&io_conf, 0, sizeof(io_conf));
    // io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    io_conf.intr_type    = GPIO_INTR_DISABLE;  // idf v4.2
    io_conf.mode         = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = BIT64(GPIO_NUM_21);
    io_conf.pull_down_en = static_cast<gpio_pulldown_t>(0);
    io_conf.pull_up_en   = static_cast<gpio_pullup_t>(0);
    gpio_config(&io_conf);
    if (enable) {
        gpio_set_level(GPIO_NUM_21, 1);
    } else {
        gpio_set_level(GPIO_NUM_21, 0);
    }
}