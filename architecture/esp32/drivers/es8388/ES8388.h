/*
    ES8388 - a ES8388 Codec driver library for ESP-IDF
    ported to esp-idf by Mathias Kirkegaard

    Copyright (C) 2020, Mathias Kirkegaard, IDMIL

    Heavily inspired by:
    https://github.com/espressif/esp-adf/tree/master/components/audio_hal/driver/es8388

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ES8388_H__
#define __ES8388_H__

#include <cstring>  // memcpy

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "esp_types.h"
#include "esxxx_common.h"

// #ifdef __cplusplus
// extern "C" {
// #endif

/* ES8388 register */
#define ES8388_CONTROL1 0x00  // 0
#define ES8388_CONTROL2 0x01  // 1

#define ES8388_CHIPPOWER 0x02  // 2

#define ES8388_ADCPOWER 0x03  // 3
#define ES8388_DACPOWER 0x04  // 4

#define ES8388_CHIPLOPOW1 0x05  // 5
#define ES8388_CHIPLOPOW2 0x06  // 6

#define ES8388_ANAVOLMANAG 0x07  // 7

#define ES8388_MASTERMODE 0x08  // 8
/* ADC */
#define ES8388_ADCCONTROL1 0x09
#define ES8388_ADCCONTROL2 0x0a
#define ES8388_ADCCONTROL3 0x0b   // 11
#define ES8388_ADCCONTROL4 0x0c   // 12
#define ES8388_ADCCONTROL5 0x0d   // 13
#define ES8388_ADCCONTROL6 0x0e   // 14
#define ES8388_ADCCONTROL7 0x0f   // 15
#define ES8388_ADCCONTROL8 0x10   // 16
#define ES8388_ADCCONTROL9 0x11   // 17
#define ES8388_ADCCONTROL10 0x12  // 18
#define ES8388_ADCCONTROL11 0x13  // 19
#define ES8388_ADCCONTROL12 0x14  // 20
#define ES8388_ADCCONTROL13 0x15  // 21
#define ES8388_ADCCONTROL14 0x16  // 22
/* DAC */
#define ES8388_DACCONTROL1 0x17   // 23
#define ES8388_DACCONTROL2 0x18   // 24
#define ES8388_DACCONTROL3 0x19   // 25
#define ES8388_DACCONTROL4 0x1a   // 26
#define ES8388_DACCONTROL5 0x1b   // 27
#define ES8388_DACCONTROL6 0x1c   // 28
#define ES8388_DACCONTROL7 0x1d   // 29
#define ES8388_DACCONTROL8 0x1e   // 30
#define ES8388_DACCONTROL9 0x1f   // 31
#define ES8388_DACCONTROL10 0x20  // 32
#define ES8388_DACCONTROL11 0x21  // 33
#define ES8388_DACCONTROL12 0x22  // 34
#define ES8388_DACCONTROL13 0x23  // 35
#define ES8388_DACCONTROL14 0x24  // 36
#define ES8388_DACCONTROL15 0x25  // 37
#define ES8388_DACCONTROL16 0x26  // 38
#define ES8388_DACCONTROL17 0x27  // 39
#define ES8388_DACCONTROL18 0x28  // 40
#define ES8388_DACCONTROL19 0x29  // 41
#define ES8388_DACCONTROL20 0x2a  // 42
#define ES8388_DACCONTROL21 0x2b  // 43
#define ES8388_DACCONTROL22 0x2c  // 44
#define ES8388_DACCONTROL23 0x2d  // 45
#define ES8388_DACCONTROL24 0x2e  // 46
#define ES8388_DACCONTROL25 0x2f  // 47
#define ES8388_DACCONTROL26 0x30  // 48
#define ES8388_DACCONTROL27 0x31  // 49
#define ES8388_DACCONTROL28 0x32  // 50
#define ES8388_DACCONTROL29 0x33  // 51
#define ES8388_DACCONTROL30 0x34  // 52

// I2C
#define I2C_MASTER_NUM 1 /*!< I2C port number for master dev */
#define I2C_MASTER_SCL_IO 23
#define I2C_MASTER_SDA_IO 18
#define I2C_MASTER_FREQ_HZ 100000
#define I2C_MASTER_TX_BUF_DISABLE 0
#define I2C_MASTER_RX_BUF_DISABLE 0
#define I2C_MASTER_TIMEOUT 1048575

#define WRITE_BIT I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ   /*!< I2C master read */
#define ACK_CHECK_EN 0x1           /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0          /*!< I2C master will not check ack from slave */

#define ACK_VAL (i2c_ack_type_t)0x0  /*!< I2C ack value */
#define NACK_VAL (i2c_ack_type_t)0x1 /*!< I2C nack value */

/* ES8388 address */
#define ES8388_ADDR 0x10  // 0010000 << 1

// I2S
#define AUDIO_HAL_MODE_SLAVE 0x00

class ES8388 {
   public:
    // Default interface settings
    // TODO read from espaudio configuration 
    es_i2s_fmt_t     fmt  = ES_I2S_NORMAL;
    es_bits_length_t bits = BIT_LENGTH_24BITS;
    es_module_t      mode = ES_MODULE_ADC_DAC;
    uint8_t div = 0x02;


    /**
     * @brief Initialize user parameters for the ES8388 driver
     *
     * @param BS buffersize 
     * @param FS Samplerate 
     * @param BIT_DEPTH the wordlength in bits 
     *
     */

	void readParams(int FS,int BIT_DEPTH);


    /**
     * @brief Initialize ES8388 codec chip
     *
     * @param cfg configuration of ES8388
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */
    esp_err_t Init(int FS, int BIT_DEPTH);

    /**
     * Initialize I2C communication with the ES8388 codec chip
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */

    esp_err_t InitI2C();

    /**
     * @brief Deinitialize ES8388 codec chip
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */
    esp_err_t DeInit();

    /**
     * @brief Configure ES8388 I2S format
     *
     * @param mod:  set ADC or DAC or both
     * @param cfg:   ES8388 I2S format
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */
    esp_err_t SetI2sFormat(es_module_t mod, es_i2s_fmt_t cfg);

    /**
     * @brief Configure I2s clock in MSATER mode
     *
     * @param cfg:  set bits clock and WS clock
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */

    /**
     * @brief Configure ES8388 codec mode and I2S interface
     *
     * @param mode codec mode
     * @param iface I2S config
     *
     * @return
     *     - ESP_FAIL Parameter error
     *     - ESP_OK   Success
     */
    esp_err_t I2SConfig();


    esp_err_t SetMclkDiv(int mclkDiv);

    esp_err_t SetI2sClock(es_i2s_clock_t cfg);

    /**
     * @brief Configure ES8388 data sample bits
     *
     * @param mode:  set ADC or DAC or both
     * @param bit_per_sample:  bit number of per sample
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */
    esp_err_t SetI2SWordSize(es_module_t mode, es_bits_length_t bit_per_sample);

    /**
     * @brief  Start ES8388 codec chip
     *
     * @param mode:  set ADC or DAC or both
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */
    esp_err_t Start(es_module_t mode);

    /**
     * @brief  Stop ES8388 codec chip
     *
     * @param mode:  set ADC or DAC or both
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */
    esp_err_t Stop(es_module_t mode);

    /**
     * @brief  Set voice volume
     *
     * @param volume:  volume (0~100)
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */
    esp_err_t SetVoiceVolume(int volume);

    /**
     * @brief Get voice volume
     *
     * @param[out] *volume: volume (0~100)
     *
     * @return
     *     - ESP_OK
     *     - ESP_FAIL
     */
    esp_err_t GetVoiceVolume(int* volume);

    /**
     * @brief Configure ES8388 DAC mute or not. Basically you can use this function to mute the output or unmute
     *
     * @param enable enable(1) or disable(0)
     *
     * @return
     *     - ESP_FAIL Parameter error
     *     - ESP_OK   Success
     */
    esp_err_t SetVoiceMute(bool enable);

    /**
     * @brief Get ES8388 DAC mute status
     *
     *  @return
     *     - ESP_FAIL Parameter error
     *     - ESP_OK   Success
     */
    esp_err_t GetVoiceMute(void);

    /**
     * @brief Set ES8388 mic gain
     *
     * @param gain db of mic gain
     *
     * @return
     *     - ESP_FAIL Parameter error
     *     - ESP_OK   Success
     */
    esp_err_t SetMicGain(es_mic_gain_t gain);

    /**
     * @brief Set ES8388 adc input mode
     *
     * @param input adc input mode
     *
     * @return
     *     - ESP_FAIL Parameter error
     *     - ESP_OK   Success
     */
    esp_err_t ConfigAdcInput(es_adc_input_t input);

    /**
     * @brief Set ES8388 dac output mode
     *
     * @param output dac output mode
     *
     * @return
     *     - ESP_FAIL Parameter error
     *     - ESP_OK   Success
     */
    esp_err_t ConfigDacOutput(es_dac_output_t output);

    /**
     * @brief Write ES8388 register
     *
     * @param reg_add address of register
     * @param data data of register
     *
     * @return
     *     - ESP_FAIL Parameter error
     *     - ESP_OK   Success
     */
    esp_err_t WriteReg(uint8_t reg_add, uint8_t val);

    /**
     * @brief Read ES8388 register
     *
     * @param reg_add address of register
     * @param data data of register
     *
     * @return
     *     - ESP_FAIL Parameter error
     *     - ESP_OK   Success
     */
    esp_err_t ReadReg(uint8_t reg_add, uint8_t* val);

    /**
     * @brief Print all ES8388 registers
     *
     * @return
     *     - void
     */
    void ReadAll();

    /**
     * @brief Set ES8388 PA power
     *
     * @param enable true for enable PA power, false for disable PA power
     *
     * @return
     *      - void
     */
    void PaPower(bool enable);

    esp_err_t SetAdcDacVolume(int mode, int volume, int dot);
};

// #ifdef __cplusplus
// }
// #endif

#endif  //__ES8388_H__
