function getDesc() {
	return '{\
	"name": "Auto_filter",\
	"filename": "Auto_filter.dsp",\
	"version": "2.23.6",\
	"compile_options": "-lang cpp -scal -ftz 0",\
	"library_list": ["/usr/local/share/faust/stdfaust.lib","/usr/local/share/faust/oscillators.lib","/usr/local/share/faust/haptic1D.lib","/usr/local/share/faust/signals.lib","/usr/local/share/faust/basics.lib","/usr/local/share/faust/noises.lib","/usr/local/share/faust/filters.lib","/usr/local/share/faust/maths.lib","/usr/local/share/faust/platform.lib","/usr/local/share/faust/vaeffects.lib","/usr/local/share/faust/delays.lib"],\
	"include_pathnames": ["/usr/local/share/faust","/usr/local/share/faust","/usr/share/faust","."],\
	"inputs": 1,\
	"outputs": 2,\
	"meta": [ \
		{ "basics.lib/name": "Faust Basic Element Library" },\
		{ "basics.lib/version": "0.1" },\
		{ "delays.lib/name": "Faust Delay Library" },\
		{ "delays.lib/version": "0.1" },\
		{ "filename": "Auto_filter.dsp" },\
		{ "filters.lib/allpassnnlt:author": "Julius O. Smith III" },\
		{ "filters.lib/allpassnnlt:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },\
		{ "filters.lib/allpassnnlt:license": "MIT-style STK-4.3 license" },\
		{ "filters.lib/lowpass0_highpass1": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },\
		{ "filters.lib/name": "Faust Filters Library" },\
		{ "filters.lib/nlf2:author": "Julius O. Smith III" },\
		{ "filters.lib/nlf2:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },\
		{ "filters.lib/nlf2:license": "MIT-style STK-4.3 license" },\
		{ "filters.lib/tf2np:author": "Julius O. Smith III" },\
		{ "filters.lib/tf2np:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },\
		{ "filters.lib/tf2np:license": "MIT-style STK-4.3 license" },\
		{ "haptic1D.lib/name": "Faust 1D Haptics Library" },\
		{ "haptic1D.lib/version": "0.0" },\
		{ "maths.lib/author": "GRAME" },\
		{ "maths.lib/copyright": "GRAME" },\
		{ "maths.lib/license": "LGPL with exception" },\
		{ "maths.lib/name": "Faust Math Library" },\
		{ "maths.lib/version": "2.2" },\
		{ "name": "Auto_filter" },\
		{ "noises.lib/name": "Faust Noise Generator Library" },\
		{ "noises.lib/version": "0.0" },\
		{ "oscillators.lib/name": "Faust Oscillator Library" },\
		{ "oscillators.lib/version": "0.0" },\
		{ "platform.lib/name": "Generic Platform Library" },\
		{ "platform.lib/version": "0.1" },\
		{ "signals.lib/name": "Faust Signal Routing Library" },\
		{ "signals.lib/version": "0.0" },\
		{ "vaeffects.lib/moog_vcf_2bn:author": "Julius O. Smith III" },\
		{ "vaeffects.lib/moog_vcf_2bn:copyright": "Copyright (C) 2003-2019 by Julius O. Smith III <jos@ccrma.stanford.edu>" },\
		{ "vaeffects.lib/moog_vcf_2bn:license": "MIT-style STK-4.3 license" },\
		{ "vaeffects.lib/name": "Faust Virtual Analog Filter Effect Library" },\
		{ "vaeffects.lib/version": "0.0" }\
	],\
	"ui": [ \
		{\
			"type": "vgroup",\
			"label": "Auto_filter",\
			"items": [ \
				{\
					"type": "hgroup",\
					"label": "0x00",\
					"meta": [\
						{ "1": "" }\
					],\
					"items": [ \
						{\
							"type": "vgroup",\
							"label": "Envelope",\
							"meta": [\
								{ "0": "" }\
							],\
							"items": [ \
								{\
									"type": "hslider",\
									"label": "Modulation Amount",\
									"address": "/Auto_filter/0x00/Envelope/Modulation_Amount",\
									"init": 0,\
									"min": 0,\
									"max": 1,\
									"step": 0.0001\
								},\
								{\
									"type": "hgroup",\
									"label": "TF0",\
									"meta": [\
										{ "bipolar": "0" },\
										{ "nPoints": "5" },\
										{ "style": "tranfunc_nPoint" }\
									],\
									"items": [ \
										{\
											"type": "hgroup",\
											"label": "Points",\
											"items": [ \
												{\
													"type": "vgroup",\
													"label": "0",\
													"items": [ \
														{\
															"type": "hslider",\
															"label": "x",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/0/x",\
															"init": 550,\
															"min": 0,\
															"max": 1100,\
															"step": 0.01\
														},\
														{\
															"type": "hslider",\
															"label": "y",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/0/y",\
															"init": 0,\
															"min": 0,\
															"max": 1,\
															"step": 0.001\
														}\
													]\
												},\
												{\
													"type": "vgroup",\
													"label": "1",\
													"items": [ \
														{\
															"type": "hslider",\
															"label": "p",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/1/p",\
															"init": 0,\
															"min": -100,\
															"max": 100,\
															"step": 0.001\
														},\
														{\
															"type": "hslider",\
															"label": "x",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/1/x",\
															"init": 1650,\
															"min": 1100,\
															"max": 2200,\
															"step": 0.01\
														},\
														{\
															"type": "hslider",\
															"label": "y",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/1/y",\
															"init": 0,\
															"min": 0,\
															"max": 1,\
															"step": 0.001\
														}\
													]\
												},\
												{\
													"type": "vgroup",\
													"label": "2",\
													"items": [ \
														{\
															"type": "hslider",\
															"label": "p",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/2/p",\
															"init": 0,\
															"min": -100,\
															"max": 100,\
															"step": 0.001\
														},\
														{\
															"type": "hslider",\
															"label": "x",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/2/x",\
															"init": 2750,\
															"min": 2200,\
															"max": 3300,\
															"step": 0.01\
														},\
														{\
															"type": "hslider",\
															"label": "y",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/2/y",\
															"init": 0,\
															"min": 0,\
															"max": 1,\
															"step": 0.001\
														}\
													]\
												},\
												{\
													"type": "vgroup",\
													"label": "3",\
													"items": [ \
														{\
															"type": "hslider",\
															"label": "p",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/3/p",\
															"init": 0,\
															"min": -100,\
															"max": 100,\
															"step": 0.001\
														},\
														{\
															"type": "hslider",\
															"label": "x",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/3/x",\
															"init": 3850,\
															"min": 3300,\
															"max": 4400,\
															"step": 0.01\
														},\
														{\
															"type": "hslider",\
															"label": "y",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/3/y",\
															"init": 0,\
															"min": 0,\
															"max": 1,\
															"step": 0.001\
														}\
													]\
												},\
												{\
													"type": "vgroup",\
													"label": "4",\
													"items": [ \
														{\
															"type": "hslider",\
															"label": "p",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/4/p",\
															"init": 0,\
															"min": -100,\
															"max": 100,\
															"step": 0.001\
														},\
														{\
															"type": "hslider",\
															"label": "x",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/4/x",\
															"init": 4950,\
															"min": 4400,\
															"max": 6600,\
															"step": 0.01\
														},\
														{\
															"type": "hslider",\
															"label": "y",\
															"address": "/Auto_filter/0x00/Envelope/TF0/Points/4/y",\
															"init": 0,\
															"min": 0,\
															"max": 1,\
															"step": 0.001\
														}\
													]\
												}\
											]\
										},\
										{\
											"type": "hbargraph",\
											"label": "output",\
											"address": "/Auto_filter/0x00/Envelope/TF0/output",\
											"min": 0,\
											"max": 1\
										},\
										{\
											"type": "hbargraph",\
											"label": "rawInput",\
											"address": "/Auto_filter/0x00/Envelope/TF0/rawInput",\
											"min": 0,\
											"max": 5500\
										}\
									]\
								}\
							]\
						},\
						{\
							"type": "vgroup",\
							"label": "Filter",\
							"meta": [\
								{ "2": "" }\
							],\
							"items": [ \
								{\
									"type": "hslider",\
									"label": "Freq",\
									"address": "/Auto_filter/0x00/Filter/Freq",\
									"init": 1000,\
									"min": 100,\
									"max": 10000,\
									"step": 1\
								},\
								{\
									"type": "hslider",\
									"label": "Q",\
									"address": "/Auto_filter/0x00/Filter/Q",\
									"init": 0.1,\
									"min": 0.1,\
									"max": 1,\
									"step": 0.0001\
								},\
								{\
									"type": "hslider",\
									"label": "Width",\
									"address": "/Auto_filter/0x00/Filter/Width",\
									"init": 0,\
									"min": 0,\
									"max": 0.5,\
									"step": 1e-05\
								}\
							]\
						},\
						{\
							"type": "vgroup",\
							"label": "Haptics",\
							"meta": [\
								{ "1": "" }\
							],\
							"items": [ \
								{\
									"type": "hslider",\
									"label": "Vibration Freq",\
									"address": "/Auto_filter/0x00/Haptics/Vibration_Freq",\
									"init": 130,\
									"min": 0,\
									"max": 300,\
									"step": 0.01\
								},\
								{\
									"type": "hslider",\
									"label": "Vibration Strength",\
									"address": "/Auto_filter/0x00/Haptics/Vibration_Strength",\
									"init": 0,\
									"min": 0,\
									"max": 150,\
									"step": 1\
								}\
							]\
						},\
						{\
							"type": "hgroup",\
							"label": "Master",\
							"meta": [\
								{ "3": "" }\
							],\
							"items": [ \
								{\
									"type": "hgroup",\
									"label": "Oscillator",\
									"items": [ \
										{\
											"type": "vslider",\
											"label": "Frequency",\
											"address": "/Auto_filter/0x00/Master/Oscillator/Frequency",\
											"meta": [\
												{ "style": "knob" }\
											],\
											"init": 20,\
											"min": 0,\
											"max": 500,\
											"step": 0.1\
										},\
										{\
											"type": "vslider",\
											"label": "Modulation",\
											"address": "/Auto_filter/0x00/Master/Oscillator/Modulation",\
											"meta": [\
												{ "style": "knob" }\
											],\
											"init": 0,\
											"min": -3,\
											"max": 3,\
											"step": 0.001\
										}\
									]\
								},\
								{\
									"type": "vgroup",\
									"label": "Route",\
									"items": [ \
										{\
											"type": "checkbox",\
											"label": "Bypass",\
											"address": "/Auto_filter/0x00/Master/Route/Bypass",\
											"meta": [\
												{ "0": "" }\
											]\
										},\
										{\
											"type": "hgroup",\
											"label": "Mixer",\
											"items": [ \
												{\
													"type": "hslider",\
													"label": "External",\
													"address": "/Auto_filter/0x00/Master/Route/Mixer/External",\
													"meta": [\
														{ "1": "" }\
													],\
													"init": 0,\
													"min": 0,\
													"max": 1,\
													"step": 0.001\
												},\
												{\
													"type": "hslider",\
													"label": "Oscillator",\
													"address": "/Auto_filter/0x00/Master/Route/Mixer/Oscillator",\
													"meta": [\
														{ "2": "" }\
													],\
													"init": 0,\
													"min": 0,\
													"max": 1,\
													"step": 0.001\
												}\
											]\
										}\
									]\
								},\
								{\
									"type": "vgroup",\
									"label": "Volume",\
									"items": [ \
										{\
											"type": "hslider",\
											"label": "Gain",\
											"address": "/Auto_filter/0x00/Master/Volume/Gain",\
											"meta": [\
												{ "style": "knob" }\
											],\
											"init": 0.5,\
											"min": 0,\
											"max": 1,\
											"step": 0.001\
										},\
										{\
											"type": "hslider",\
											"label": "Wet",\
											"address": "/Auto_filter/0x00/Master/Volume/Wet",\
											"meta": [\
												{ "style": "knob" }\
											],\
											"init": 1,\
											"min": 0,\
											"max": 1,\
											"step": 0.0001\
										}\
									]\
								}\
							]\
						}\
					]\
				},\
				{\
					"type": "vgroup",\
					"label": "TorqueTuner0",\
					"meta": [\
						{ "deviceId": "0" },\
						{ "hidden": "1" },\
						{ "style": "haptic_dev" }\
					],\
					"items": [ \
						{\
							"type": "hbargraph",\
							"label": "Force",\
							"address": "/Auto_filter/TorqueTuner0/Force",\
							"min": -7.775,\
							"max": 7.775\
						},\
						{\
							"type": "hslider",\
							"label": "Pos",\
							"address": "/Auto_filter/TorqueTuner0/Pos",\
							"init": 0,\
							"min": 0,\
							"max": 360,\
							"step": 0.1\
						}\
					]\
				}\
			]\
		}\
	]\
}';
}
