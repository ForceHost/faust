    {
        /*
      TODO:
        - Implement Hidden
        - Implement [style: led]
        - Implement [unit: db]
        - Implement [unit: xxx]
        - Implement [tooltip: xxx]
        - Implement menu
        - It would be much more efficient to set parametefsliderrs with their ID instead
        of full address at least for the electron version.
    */

        var osc = require("osc");
        var Nexus = require("nexusui");
        var event = require("events");
        var cloneDeep = require("lodash/cloneDeep");

        var host_is_local = false;
        var has_connection = true;

        // For debugging gui without having to re-compile esp firmware
        var ip = '';
        if (host_is_local) {
            ip = '192.168.1.42'
        } else {
            ip = location.hostname;
        }

        // MetaData initialization (retrieved in recieving JSONUI)
        if (has_connection) {
            var ws = new WebSocket('ws://' + ip + '/');
            var oscPort = new osc.WebSocketPort({
                url: 'ws://' + ip + '/', // URL to your Web Socket server.
                // socket: ws,
                metadata: true
            });

            oscPort.open();

            oscPort.on("message", function(oscMsg) {
                if (typeof(oscMsg.address) !== "undefined" && typeof(callBackFuncs[oscMsg.address]) === "function") {

                    // Find the corresponding callback function
                    execFunc = callBackFuncs[oscMsg.address];

                    // Execute it with the recieved value as argument
                    execFunc(oscMsg.args[0].value);
                }
            });

            oscPort.on("ready", function() {
                // console.log("ready");
                oscPort.readyState = true;
            });

            oscPort.on("error", function(error) {
                console.log("An error occurred: ", error.message);
            });

            function unpack(evt) {
                var msg = evt.data.split('\'');
                var jsonstring = JSON.stringify({
                    'path': msg[1],
                    'value': msg[2]
                });
                return JSON.parse(jsonstring);
            }
        } else {
            oscPort = {
                readyState: false
            }
        }


        var callBackFuncs = {};

        function httpGetAsync(theUrl, callback) {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                    callback(xmlHttp.responseText);
            }
            xmlHttp.open("GET", theUrl, true); // true for asynchronous 
            xmlHttp.send(null);
        }

        function FaustUI() {
            var depth  =  0;
            var appName = "";
            // var defaultColor = [150, 150, 150];
            var currentID = 0;
            var mainDiv = document.createElement("div");
            mainDiv.setAttribute("id", "faustUI");
            document.body.appendChild(mainDiv);
            var faustJSONUI;

            if (!has_connection) {
                let jsondata = getDesc();
                console.log(jsondata);
                faustJSONUI = JSON.parse(jsondata);
                retrieveMetaData(faustJSONUI);
                parseFaustUI(faustJSONUI.ui, mainDiv);
            } else {
                httpGetAsync("http://" + ip + "/JSON",
                    function(jsondata) {
                        console.log("json recieved");
                        console.log(jsondata);
                        var faustJSONUI = JSON.parse(jsondata);
                        retrieveMetaData(faustJSONUI);
                        parseFaustUI(faustJSONUI.ui, mainDiv);
                    });
            }
            console.log("(faustui)  appName = " + appName);

            return mainDiv;

            function createUIElement(parent, localJSON) {
                var elementDiv = document.createElement("div");
                elementDiv.setAttribute("class", "uiElement");
                elementDiv.setAttribute("id", localJSON.type + "-" + localJSON.label.replace(" ", "-"));
                // elementDiv.style.backgroundColor = "rgb(" + defaultColor[0] + "," + defaultColor[1] + "," + defaultColor[2] + ")";
                elementDiv.style.backgroundColor = defaultColor;
                if (localJSON.label != "0x00") {
                    var labelDiv = document.createElement("div");
                    if(depth == 1){
                        labelDiv.setAttribute("class", "title");
                        labelDiv.innerHTML = appName;
                        // labelDiv.style.fontSize = 150 + '%';
                    } else {
                        labelDiv.setAttribute("class", "label");
                        labelDiv.innerHTML = localJSON.label;
                    }
                    
                    // var textColor = [0, 0, 0];
                    // for (i = 0; i < 3; i++) {
                    //     textColor[i] = defaultColor[i] - 50;
                    // }
                    // labelDiv.style.color = "rgb(" + textColor[0] + "," + textColor[1] + "," + textColor[2] + ")";
                    // labelDiv.style.backgroundColor = "rgb(" + defaultColor[0] + "," + defaultColor[1] + "," + defaultColor[2] + ")";
                    labelDiv.style.backgroundColor = defaultColor;
                    elementDiv.appendChild(labelDiv);
                }
                parent.appendChild(elementDiv);
                return elementDiv;
            }

            function Vgroup(parentDiv) {
                var group = document.createElement("div");
                group.setAttribute("class", "vgroup");
                parentDiv.appendChild(group);
                return group;
            }

            function Hgroup(parentDiv) {
                var group = document.createElement("div");
                group.setAttribute("class", "hgroup");
                parentDiv.appendChild(group);
                return group;
            }

            // function essentially behaving like a class
            function Hslider(parentDiv, curJSON) {
                var hslider = document.createElement("div");
                hslider.setAttribute("class", "hslider");
                hslider.value = Number(curJSON.init);
                hslider.min = Number(curJSON.min);
                hslider.max = Number(curJSON.max);
                hslider.range = Math.abs(hslider.max - hslider.min);
                hslider.step = Number(curJSON.step);
                hslider.decimals = Math.ceil(Math.log10(1/hslider.step));
                hslider.address = curJSON.address;
                hslider.faustID = currentID;
                currentID++;
                hslider.clicked = 0;
                hslider.hasChanged = false;
                hslider.updateRate = 30;

                var sliderValue = document.createElement("div");
                hslider.appendChild(sliderValue);
                sliderValue.setAttribute("class", "value");
                sliderValue.setAttribute("id", curJSON.address + "-val");
                sliderValue.innerHTML += curJSON.init;

                var sliderBar = document.createElement("div");
                hslider.appendChild(sliderBar);
                sliderBar.setAttribute("class", "bar");
                sliderBar.addEventListener("mousedown", sliderClickDown, false);
                // mouse move and mouse up on all page to catch drag events outside of div
                document.addEventListener("mousemove", sliderMove, false);
                document.addEventListener("mouseup", sliderClickUp, false);

                var sliderCursor = document.createElement("div");
                sliderBar.appendChild(sliderCursor);
                sliderCursor.setAttribute("class", "cursor");


                hslider.setNormValue = function(value) {
                    if (value >= 0 && value <= 1) {
                        var cursorXPos = value * 100 + "%";
                        sliderCursor.style.left = cursorXPos;
                        hslider.value = value * hslider.range + hslider.min;
                        hslider.value = Math.floor(hslider.value / hslider.step) * hslider.step;
                        sliderValue.innerHTML = hslider.value.toFixed(hslider.decimals);
                        hslider.hasChanged = true;
                    }
                }

                var cursorNormXPos = hslider.value / hslider.range - hslider.min / hslider.range;
                hslider.setNormValue(cursorNormXPos);

                function updateCallback() {
                    if (oscPort.readyState == true && hslider.hasChanged) {
                        hslider.hasChanged = false;
                        oscPort.send({
                            address: hslider.address,
                            args: [{
                                type: "f",
                                value: hslider.value,
                            }]
                        });
                    }
                }

                setInterval(updateCallback, hslider.updateRate);

                function extUpdateCallback(value) {
                    hslider.value = value;
                    sliderValue.innerHTML = hslider.value.toFixed(2);
                    // var cursorXPos = ((value - hslider.min) / hslider.range) * 100 + "%";
                    // sliderCursor.style.left = cursorXPos;
                }
                callBackFuncs[hslider.address] = extUpdateCallback;

                function sliderClickDown(e) {
                    var cursorPos = (e.clientX - this.offsetLeft) / this.offsetWidth;
                    hslider.clicked = 1;
                    hslider.setNormValue(cursorPos);
                }

                function sliderClickUp(e) {
                    if (hslider.clicked == 1) {
                        hslider.clicked = 0;
                    }
                }

                function sliderMove(e) {
                    if (hslider.clicked == 1) {
                        var cursorPos = (e.clientX - sliderBar.offsetLeft) / sliderBar.offsetWidth;
                        hslider.setNormValue(cursorPos);
                    }
                }

                parentDiv.appendChild(hslider);
                return hslider;
            }

            function Vslider(parentDiv, curJSON) {
                var vslider = document.createElement("div");
                vslider.setAttribute("class", "vslider");
                vslider.value = Number(curJSON.init);
                vslider.min = Number(curJSON.min);
                vslider.max = Number(curJSON.max);
                vslider.range = Math.abs(vslider.max - vslider.min);
                vslider.step = Number(curJSON.step);
                vslider.decimals = Math.ceil(Math.log10(1/vslider.step));
                vslider.address = curJSON.address;
                vslider.faustID = currentID;
                currentID++;
                vslider.clicked = false;
                vslider.hasChaned = false;
                vslider.updateRate = 30;
                vslider.paramValue = 0;

                var sliderValue = document.createElement("div");
                sliderValue.setAttribute("class", "value");
                sliderValue.setAttribute("id", curJSON.address + "-val");
                sliderValue.innerHTML += curJSON.init;

                var sliderBar = document.createElement("div");
                vslider.appendChild(sliderBar);
                sliderBar.setAttribute("class", "bar");
                sliderBar.addEventListener("mousedown", sliderClickDown, false);

                // mouse move and mouse up on all page to catch drag events outside of div
                document.addEventListener("mousemove", sliderMove, false);
                document.addEventListener("mouseup", sliderClickUp, false);

                vslider.appendChild(sliderValue);

                var sliderCursor = document.createElement("div");
                sliderBar.appendChild(sliderCursor);
                sliderCursor.setAttribute("class", "cursor");

                vslider.setNormValue = function(v) {
                    if (v >= 0 && v <= 1) {
                        var cursorYPos = (1 - v) * 100 + "%";
                        sliderCursor.style.top = cursorYPos;
                        vslider.value = v * vslider.range + vslider.min;
                        vslider.value = Math.floor(vslider.value / vslider.step) * vslider.step;
                        sliderValue.innerHTML = vslider.value.toFixed(vslider.decimals);
                        vslider.hasChaned = true;
                    }
                }

                function updateCallback() {
                    if (oscPort.readyState == true && vslider.hasChanged) {
                        vslider.hasChanged = false;
                        oscPort.send({
                            address: vslider.address,
                            args: [{
                                type: "f",
                                value: vslider.value,
                            }]
                        });
                    }
                }

                setInterval(updateCallback, vslider.updateRate);

                function extUpdateCallback(value) {
                    vslider.value = value;
                    sliderValue.innerHTML = vslider.value.toFixed(2);
                    var cursorXPos = ((vslider.value - vslider.min) / vslider.range) * 100 + "%";
                    sliderCursor.style.left = cursorXPos;
                }

                callBackFuncs[vslider.address] = extUpdateCallback;

                var cursorNormYPos = vslider.value / vslider.range - vslider.min / vslider.range;
                vslider.setNormValue(cursorNormYPos);

                function sliderClickDown(e) {
                    var cursorPos = 1 - (e.clientY - this.offsetTop) / this.offsetHeight;
                    vslider.clicked = true;
                    vslider.setNormValue(cursorPos);
                }

                function sliderClickUp(e) {
                    if (vslider.clicked == true) {
                        vslider.clicked = false;
                    }
                }

                function sliderMove(e) {
                    if (vslider.clicked == true) {
                        var cursorPos = 1 - (e.clientY - sliderBar.offsetTop) / sliderBar.offsetHeight;
                        vslider.setNormValue(cursorPos);
                    }
                }
                parentDiv.appendChild(vslider);
                return vslider;
            }

            // function essentially behaving like a class
            function Knob(parentDiv, curJSON) {
                // TODO: ignoring step for now
                var knob = document.createElement("div");
                knob.setAttribute("class", "knob");
                knob.value = Number(curJSON.init);
                knob.min = Number(curJSON.min);
                knob.max = Number(curJSON.max);
                knob.range = Math.abs(knob.max - knob.min);
                knob.step = Number(curJSON.step);
                knob.decimals = Math.ceil(Math.log10(1/knob.step));
                knob.address = curJSON.address;
                knob.faustID = currentID;
                currentID++;
                knob.clicked = 0;
                knob.clickOriginY = 0;
                knob.clickOrigAngle = 0;
                knob.handleAngle = 0;
                knob.hasChanged = false;
                knob.updateRate = 30;

                var knobValue = document.createElement("div");
                knobValue.setAttribute("class", "value");
                knobValue.setAttribute("id", curJSON.address + "-val");
                knobValue.innerHTML += curJSON.init;

                var knobBase = document.createElement("div");
                knob.appendChild(knobBase);
                knobBase.setAttribute("class", "base");
                knobBase.addEventListener("mousedown", knobClickDown, false);
                // mouse move and mouse up on all page to catch drag events outside of div
                document.addEventListener("mousemove", knobMove, false);
                document.addEventListener("mouseup", knobClickUp, false);

                knob.appendChild(knobValue);

                var knobHandle = document.createElement("div");
                knobBase.appendChild(knobHandle);
                knobHandle.setAttribute("class", "handle");
                var knobHandleTip = document.createElement("div");
                knobHandle.appendChild(knobHandleTip);
                knobHandleTip.setAttribute("class", "tip");

                function callback(value) {
                    // set value
                }
                callBackFuncs[knob.address] = callback;

                knob.setNormValue = function(v) {
                    if (v >= 0 && v <= 1) {
                        knob.handleAngle = v * 360 - 90;
                        knobHandle.style.transform = "rotate(" + knob.handleAngle + "deg)";
                        knob.value = v * knob.range + knob.min;
                        knob.value = Math.floor(knob.value / knob.step) * knob.step;
                        // knobValue.innerHTML = knob.value.toFixed(knob.decimals);
                        knobValue.innerHTML = knob.value.toFixed(2);
                        knob.hasChanged = true;
                    }
                }

                function updateCallback() {
                    if (oscPort.readyState == true && knob.hasChanged) {
                        knob.hasChanged = false;
                        oscPort.send({
                            address: knob.address,
                            args: [{
                                type: "f",
                                value: knob.value,
                            }]
                        });
                    }
                }

                setInterval(updateCallback, knob.updateRate);

                function extUpdateCallback(value) {
                    knob.value = value;
                    knobValue.innerHTML = knob.value.toFixed(2);
                    // var cursorXPos = ((value - knob.min) / knob.range) * 100 + "%";
                    // sliderCursor.style.left = cursorXPos;
                }
                callBackFuncs[knob.address] = extUpdateCallback;

                var handleNormYPos = knob.value / knob.range - knob.min / knob.range;
                knob.setNormValue(handleNormYPos);

                function knobClickDown(e) {
                    knob.clicked = 1;
                    knob.clickOriginY = e.clientY;
                    knob.clickOrigAngle = knob.handleAngle;
                }

                function knobClickUp(e) {
                    if (knob.clicked == 1) {
                        knob.clicked = 0;
                    }
                }

                function knobMove(e) {
                    if (knob.clicked == 1) {
                        deltaY = knob.clickOriginY - e.clientY;
                        knob.setNormValue((knob.clickOrigAngle + deltaY + 90) / 360);
                    }
                }
                parentDiv.appendChild(knob);
                return knob;
            }

            function Nentry(parentDiv, curJSON) {
                var nentry = document.createElement("input");
                nentry.setAttribute("type", "number");
                nentry.setAttribute("class", "nentry");
                nentry.setAttribute("min", curJSON.min);
                nentry.setAttribute("max", curJSON.max);
                nentry.setAttribute("step", curJSON.step);
                nentry.setAttribute("value", curJSON.init);
                nentry.hasChanged  =  false;
                nentry.address = curJSON.address;
                nentry.faustID = currentID;
                currentID++;
                nentry.updateRate = 100;
                nentry.value = curJSON.init;
                nentry.addEventListener("change", onValueChanged, false);

                function onValueChanged(e) {
                    nentry.hasChanged  =  true;
                    nentry.value = e.target.value;
                }

                function updateCallback() {
                    if (oscPort.readyState == true && nentry.hasChanged) {
                        nentry.hasChanged = false;
                        oscPort.send({
                            address: nentry.address,
                            args: [{
                                type: "f",
                                value: nentry.value,
                            }]
                        });
                    }
                }

                setInterval(updateCallback, nentry.updateRate);

                function extUpdateCallback(value) {
                    // nentry.value = value;
                    nentry.setAttribute("value", value);
                }

                callBackFuncs[nentry.address] = extUpdateCallback;

                parentDiv.appendChild(nentry);
                return nentry;
            }

            function Button(parentDiv, curJSON) {
                var button = document.createElement("input");
                button.setAttribute("type", "button");
                button.setAttribute("class", "button");
                button.setAttribute("value", curJSON.label);
                button.address = curJSON.address;
                button.faustID = currentID;
                currentID++;
                button.addEventListener("mousedown", onMouseDown, false);
                button.addEventListener("mouseup", onMouseUp, false);

                function callback(value) {
                    // set value
                }
                callBackFuncs[button.address] = callback;

                function onMouseDown(e) {
                    if (oscPort.readyState == true) {
                        oscPort.send({
                            address: button.address,
                            args: [{
                                type: "i",
                                value: 0,
                            }]
                        });
                    }
                }

                function onMouseUp(e) {

                    if (oscPort.readyState == true) {
                        oscPort.send({
                            address: button.address,
                            args: [{
                                type: "i",
                                value: 1,
                            }]
                        });
                    }
                }
                parentDiv.appendChild(button);
                return button;
            }

            function Checkbox(parentDiv, curJSON) {
                var status = 0;
                var checkbox = document.createElement("input");
                // var center = document.createElement("div");
                // center.setAttribute("class","center");
                // center.appendChild(checkbox);
                checkbox.setAttribute("type", "button");
                checkbox.setAttribute("class", "button");
                checkbox.setAttribute("value", curJSON.label);
                checkbox.address = curJSON.address;
                checkbox.faustID = currentID;
                checkbox.status = 0;
                currentID++;
                checkbox.addEventListener("mousedown", onMouseDown, false);

                function callback(value) {
                    // set value
                }
                callBackFuncs[checkbox.address] = callback;

                function onMouseDown(e) {
                    if (status == 0) {
                        status = 1;
                        checkbox.style.backgroundColor = "var(--accent)";
                    } else if (status == 1) {
                        status = 0;
                        checkbox.style.backgroundColor = "white";
                    }
                    if (oscPort.readyState == true) {
                        oscPort.send({
                            address: checkbox.address,
                            args: [{
                                type: "i",
                                value: status,
                            }]
                        });
                    }
                }

                parentDiv.appendChild(checkbox);
                return checkbox;
            }

            function Hbargraph(parentDiv, curJSON) {
                var hbargraph = document.createElement("div");
                hbargraph.setAttribute("class", "hbargraph");
                hbargraph.min = Number(curJSON.min);
                hbargraph.max = Number(curJSON.max);
                hbargraph.range = Math.abs(hbargraph.max - hbargraph.min);
                hbargraph.address = curJSON.address;
                hbargraph.faustID = currentID;
                currentID++;
                hbargraph.updateRate = 100; // ms

                var bargraphValue = document.createElement("div");
                hbargraph.appendChild(bargraphValue);
                bargraphValue.setAttribute("class", "value");
                bargraphValue.setAttribute("id", curJSON.address + "-val");
                bargraphValue.innerHTML += 0;

                var barback = document.createElement("div");
                barback.setAttribute("class", "barback");
                hbargraph.appendChild(barback);


                var bartop = document.createElement("div");
                bartop.setAttribute("class", "bartop");
                barback.appendChild(bartop);

                function callback(value) {
                    var barPosPc = (value / hbargraph.range - hbargraph.min / hbargraph.range) * 100;
                    bartop.style.width = barPosPc + "%";
                    bargraphValue.innerHTML = value.toFixed(3);
                    return hbargraph;
                }
                callBackFuncs[hbargraph.address] = callback;

                parentDiv.appendChild(hbargraph);
                return hbargraph;
            }

            function Vbargraph(parentDiv, curJSON) {
                var vbargraph = document.createElement("div");
                vbargraph.setAttribute("class", "vbargraph");
                vbargraph.min = Number(curJSON.min);
                vbargraph.max = Number(curJSON.max);
                vbargraph.range = Math.abs(vbargraph.max - vbargraph.min);
                vbargraph.address = curJSON.address;
                vbargraph.faustID = currentID;
                currentID++;
                vbargraph.updateRate = 50; // ms

                var bargraphValue = document.createElement("div");
                hbargraph.appendChild(bargraphValue);
                bargraphValue.setAttribute("class", "value");
                bargraphValue.setAttribute("id", curJSON.address + "-val");
                bargraphValue.innerHTML += curJSON.init;


                var barback = document.createElement("div");
                barback.setAttribute("class", "barback");
                vbargraph.appendChild(barback);

                var bartop = document.createElement("div");
                bartop.setAttribute("class", "bartop");
                barback.appendChild(bartop);

                function callback(value) {
                    var barPosPc = (value / vbargraph.range - vbargraph.min / vbargraph.range) * 10;
                    bartop.style.width = barPosPc + "%";
                    return vbargraph;
                }

                callBackFuncs[vbargraph.address] = callback;

                parentDiv.appendChild(vbargraph);
                return vbargraph;
            }


            function TransferFunction_canvas(parentDiv, curJSON) {
                let tf = document.createElement("div");
                tf.setAttribute("id", "tf");
                tf.setAttribute("class", "transferfunction");
                tf.updateRate = 200;
                let tfback = document.createElement("div");
                tf.faustID = currentID;
                currentID++;
                tfback.setAttribute("id", tf.faustID);
                tfback.setAttribute("class", "tfback");
                tf.appendChild(tfback);
                parentDiv.appendChild(tf);

                tf.nPoints = 0;
                for (let meta of curJSON.meta) {
                    if (typeof meta.nPoints !== "undefined") {
                        tf.nPoints = meta.nPoints;
                    }
                    if (typeof meta.bipolar !== "undefined") {
                        if (meta.bipolar == 0) {
                            tf.bipolar = false;
                        } else {
                            tf.bipolar = true;
                        }
                    }
                }

                tf.points = [];
                let rawX = curJSON.items.find(el => el.label == "rawInput");
                if (rawX !== "undefined") {
                    tf.xMin = rawX.min;
                    tf.xMax = rawX.max;
                    tf.xRange = (tf.xMax - tf.xMin);
                    tf.inputAddress = rawX.address;
                }

                let rawY = curJSON.items.find(el => el.label == "output");
                if (rawY !== "undefined") {
                    tf.yMin = rawY.min;
                    tf.yMax = rawY.max;
                    tf.yRange = (tf.yMax - tf.yMin);
                    tf.outputAddress = rawY.address;
                }

                for (property of curJSON.items) {
                    if (property.label == "Points") {
                        let points = property.items;
                        for (let point of points) {

                            // set index
                            let yItem = point.items.find(el => el.label == "y");
                            console.log(yItem.address);
                            let y = {
                                idx: point.label,
                                address: yItem.address,
                                value: normalize(yItem.init, tf.yRange, tf.yMin),
                                min: normalize(yItem.min, tf.yRange, tf.yMin),
                                max: normalize(yItem.max, tf.yRange, tf.yMin),
                                changed: false
                            };
                            tf.points.push(y);
                        }
                    }
                }

                let values = [];
                for (let point of tf.points) {
                    console.log("y = " + point.value);
                    values.push(point.value);
                }

                let tfwidget = new Nexus.Multislider(tfback.id, {
                    'values': values,
                    'numberOfSliders': tf.points.length,
                    'min': tf.yMin,
                    'max': tf.yMax,
                    'candycane': 3,
                    'smoothing': 3,
                    'mode': 'line', // 'bar' or 'line'
                    'showAxis': tf.bipolar
                });

                let element = document.getElementById(tfback.id);
                let style = window.getComputedStyle(element);
                let fill = style.getPropertyValue('background');
                let accent = style.getPropertyValue('var(--accent)');

                tfwidget.colorize("accent", 'var(--accent)');

                // let backgroundColor = "rgb(" + defaultColor[0] + "," + defaultColor[1] + "," + defaultColor[2] + ")";
                let backgroundColor = defaultColor;
                tfwidget.colorize("fill", fill);
                tfwidget.colorize("axis", backgroundColor);

                tfwidget.hasChanged = false;
                tfwidget.hasReleased = false;

                tfwidget.on('change', function(points) {
                    tfwidget.hasChanged = true;
                    console.log('tfwidget change');
                    for (let point of tf.points) {
                        let value = points[point.idx];
                        value = scale(value, tf.yRange, tf.yMin);
                        if (point.value != value) {
                            point.value = value;
                            point.changed = true;
                        }
                    }
                }, false);

                tfwidget.on('release', function() {
                    tfwidget.hasReleased = true;
                    console.log('tfwidget release');
                }, false);

                function updateCallback() {
                    if (has_connection) {
                        if (oscPort.readyState == true && tfwidget.hasChanged && tfwidget.hasReleased) {
                            tfwidget.hasChanged = false;
                            tfwidget.hasReleased = false;
                            console.log('tfwidget send');

                            var bundle = {
                                timeTag: osc.timeTag(0),
                                packets: []
                            }

                            for (let point of tf.points) {
                                if (point.changed == true) {
                                    point.changed = false;
                                    bundle.packets.push({
                                        address: point.address,
                                        args: [{
                                            type: "f",
                                            value: point.value,
                                        }]
                                    });
                                }
                            }
                            oscPort.send(bundle);

                        }
                    }

                }

                setInterval(updateCallback, tf.updateRate);

                function xUpdateCallback(value) {
                    // TODO set real y value
                    var x = normalize(value, tf.xRange, tf.xMin);
                    tfwidget.setDisplay(x, null);
                }

                callBackFuncs[tf.inputAddress] = xUpdateCallback;

                function yUpdateCallback(value) {
                    // TODO set real y value
                    var y = normalize(value, tf.yRange, tf.yMin);
                    tfwidget.setDisplay(null, y);
                }

                callBackFuncs[tf.outputAddress] = yUpdateCallback;

                // TODO: optimize with eventlistener on resize, instead of periodically updaing
                tf.width = 0;
                tf.height = 0;

                function resize() {
                    let width = parseFloat(window.getComputedStyle(tf, null).getPropertyValue("width").replace("px", ""));
                    let height = parseFloat(window.getComputedStyle(tf, null).getPropertyValue("height").replace("px", ""));
                    if (width != tf.width || height != tf.height) {
                        tf.width = width;
                        tf.height = height;
                        tfwidget.resize(tf.width, tf.height);
                    }
                }

                setInterval(resize, 50);

                // Functions to handle conversion and transmission of tf data
                function normalize(value, range, offset) {
                    // TODO CLIP
                    return (value - offset) / range;
                }

                function scale(value, range, offset) {
                    // TODO CLIP
                    return value * range + offset;
                }

                return tf;
            }

            function TransferFunction_breakpoint(parentDiv, curJSON) {
                let tf = document.createElement("div");
                tf.setAttribute("id", "tf");
                tf.setAttribute("class", "transferfunction");
                tf.updateRate = 200;
                let tfback = document.createElement("div");
                tf.faustID = currentID;
                currentID++;
                tfback.setAttribute("id", tf.faustID);
                tfback.setAttribute("class", "tfback");
                tf.appendChild(tfback);
                parentDiv.appendChild(tf);

                tf.nPoints = 0;
                for (let meta of curJSON.meta) {
                    if (typeof meta.nPoints !== "undefined") {
                        tf.nPoints = meta.nPoints;
                    }
                    if (typeof meta.bipolar !== "undefined") {
                        if (meta.bipolar == 0) {
                            tf.bipolar = false;
                        } else {
                            tf.bipolar = true;
                        }
                    }

                }

                tf.points = [];
                let rawX = curJSON.items.find(el => el.label == "rawInput");
                if (rawX !== "undefined") {
                    tf.xMin = rawX.min;
                    tf.xMax = rawX.max;
                    tf.xRange = (tf.xMax - tf.xMin);
                    tf.inputAddress = rawX.address;
                }

                let rawY = curJSON.items.find(el => el.label == "output");
                if (rawY !== "undefined") {
                    tf.yMin = rawY.min;
                    tf.yMax = rawY.max;
                    tf.yRange = (tf.yMax - tf.yMin);
                    tf.outputAddress = rawY.address;
                }

                for (property of curJSON.items) {
                    if (property.label == "Points") {
                        let points = property.items;
                        for (let point of points) {

                            // set index
                            let idx = point.label;
                            let xItem = point.items.find(el => el.label == "x");
                            let x = {
                                address: xItem.address,
                                value: normalize(xItem.init, tf.xRange, tf.xMin),
                                min: normalize(xItem.min, tf.xRange, tf.xMin),
                                max: normalize(xItem.max, tf.xRange, tf.xMin)
                            };
                            // // callBackFuncs[x] = tfwidget.movePoint(idx,);
                            let yItem = point.items.find(el => el.label == "y");
                            let y = {
                                address: yItem.address,
                                value: normalize(yItem.init, tf.yRange, tf.yMin),
                                min: normalize(yItem.min, tf.yRange, tf.yMin),
                                max: normalize(yItem.max, tf.yRange, tf.yMin)
                            };
                            let pItem = point.items.find(el => el.label == "p");
                            if (typeof(pItem) !== "undefined") {
                                let p = {
                                    address: pItem.address,
                                    value: pItem.init,
                                    min: pItem.min,
                                    max: pItem.max
                                };
                                tf.points.push({
                                    idx: idx,
                                    x_address: x.address,
                                    x: x.value,
                                    y_address: y.address,
                                    y: y.value,
                                    xMin: x.min,
                                    xMax: x.max,
                                    yMin: y.min,
                                    yMax: y.max,
                                    p_address: p.address,
                                    p: p.value,
                                    changed: false
                                });
                            } else {
                                tf.points.push({
                                    idx: idx,
                                    x_address: x.address,
                                    x: x.value,
                                    y_address: y.address,
                                    y: y.value,
                                    xMin: x.min,
                                    xMax: x.max,
                                    yMin: y.min,
                                    yMax: y.max,
                                    changed: false
                                });
                            }
                        }

                    }
                }

                let tfwidget = new Nexus.Envelope(tfback.id, {
                    'noNewPoints': false,
                    'points': tf.points,
                    'maxPoints': tf.points.length,
                    'showAxis': tf.bipolar,
                    'curveMode': "SC",
                });

                let element = document.getElementById(tfback.id);
                let style = window.getComputedStyle(element);
                let fill = style.getPropertyValue('background');

                tfwidget.colorize("accent", 'var(--accent)');

                tfwidget.colorize("fill", fill);
                tfwidget.colorize("axis", "var(--textColor)");
                tfwidget.hasChanged = false;
                tfwidget.hasReleased = false;

                tfwidget.on('change', function(points) {
                    tfwidget.hasChanged = true;
                    for (let i in tf.points) {
                        let point = tf.points.find(el => el.idx == i);
                        let readPoint = points[i];
                        // Set remaining points to same value
                        if (i >= points.length) {
                            readPoint = points[points.length - 1];
                            point.changed = true;
                        }
                        let x = scale(readPoint.x, tf.xRange, tf.xMin);
                        if (point.x != x) {
                            point.x = x;
                            point.changed = true;
                        }
                        let y = scale(readPoint.y, tf.yRange, tf.yMin);
                        if (point.y != y) {
                            point.y = y;
                            point.changed = true;
                        }
                        if (typeof(point.p) !== "undefined") {
                            if (point.p != readPoint.p) {
                                point.p = readPoint.p;
                                point.changed = true;
                            }
                        }
                    }
                }, false);

                tfwidget.on('release', function() {
                    tfwidget.hasReleased = true;
                    console.log('tfwidget release');
                }, false);

                function updateCallback() {
                    if (has_connection) {
                        if (oscPort.readyState == true && tfwidget.hasChanged && tfwidget.hasReleased) {
                            tfwidget.hasChanged = false;
                            tfwidget.hasReleased = false;

                            var bundle = {
                                timeTag: osc.timeTag(0),
                                packets: []
                            }

                            for (let point of tf.points) {
                                if (point.changed == true) {
                                    point.changed = false;
                                    bundle.packets.push({
                                        address: point.x_address,
                                        args: [{
                                            type: "f",
                                            value: point.x,
                                        }]
                                    });
                                    bundle.packets.push({
                                        address: point.y_address,
                                        args: [{
                                            type: "f",
                                            value: point.y,
                                        }]
                                    });
                                    if (typeof(point.p) !== "undefined") {
                                        bundle.packets.push({
                                            address: point.p_address,
                                            args: [{
                                                type: "f",
                                                value: point.p,
                                            }]
                                        });
                                    }
                                }
                            }
                            // TODO: bundle all outgoing messages and transmit at fixed rate
                            oscPort.send(bundle);

                        }
                    }
                }

                setInterval(updateCallback, tf.updateRate);

                function xUpdateCallback(value) {
                    // TODO set real y value
                    var x = normalize(value, tf.xRange, tf.xMin);
                    tfwidget.setDisplay(x, null);
                }

                callBackFuncs[tf.inputAddress] = xUpdateCallback;

                function yUpdateCallback(value) {
                    // TODO set real y value
                    var y = normalize(value, tf.yRange, tf.yMin);
                    tfwidget.setDisplay(null, y);
                }

                callBackFuncs[tf.outputAddress] = yUpdateCallback;

                // TODO: optimize with eventlistener on resize, instead of periodically updaing
                tf.width = 0;
                tf.height = 0;

                function resize() {
                    let width = parseFloat(window.getComputedStyle(tf, null).getPropertyValue("width").replace("px", ""));
                    let height = parseFloat(window.getComputedStyle(tf, null).getPropertyValue("height").replace("px", ""));
                    if (width != tf.width || height != tf.height) {
                        tf.width = width;
                        tf.height = height;
                        tfwidget.resize(tf.width, tf.height);
                    }
                }

                setInterval(resize, 50);

                // Functions to handle conversion and transmission of tf data
                function normalize(value, range, offset) {
                    // TODO CLIP
                    return (value - offset) / range;
                }

                function scale(value, range, offset) {
                    // TODO CLIP
                    return value * range + offset;
                }

                return tf;
            }


            function retrieveMetaData(JSONMeta) {
                appName = JSONMeta.name.replace(/_/g, ' ');
            }

            function parseFaustUI(curJSON, curDiv) {
                depth += 1;
                if(depth % 2 == 1){
                    defaultColor = 'var(--light-gray)';
                } else {
                    defaultColor = 'var(--lighter-gray)';
                }
                for (x in curJSON) {
                console.log("Depth = " + depth + "   curJson = " + curJSON[x].label)
                    // for UI elements configured with metadata
                    var foundMetaStyle = false;
                    if (curJSON[x].meta != null) {
                        for (y in curJSON[x].meta) {
                            if (curJSON[x].meta[y].style == "knob") {
                                parentDiv = createUIElement(curDiv, curJSON[x]);
                                var knob = Knob(parentDiv, curJSON[x]);
                                foundMetaStyle = true;
                            } else if (curJSON[x].meta[y].style == "tranfunc_nPoint") {
                                parentDiv = createUIElement(curDiv, curJSON[x]);
                                var transferfunction = TransferFunction_breakpoint(parentDiv, curJSON[x]);
                                foundMetaStyle = true;
                            } else if (curJSON[x].meta[y].style == "tranfunc_canvas") {
                                parentDiv = createUIElement(curDiv, curJSON[x]);
                                var transferfunction = TransferFunction_canvas(parentDiv, curJSON[x]);
                                foundMetaStyle = true;
                            } else if(curJSON[x].meta[y].hidden == "1"){
                                // foundMetaStyle = true;
                            }  
                        }
                    }
                    // for UI elements configured with primitives
                    if (!foundMetaStyle) {
                        if (curJSON[x].type == "hgroup") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var group = Hgroup(parentDiv);
                            parseFaustUI(curJSON[x].items, group);
                        } else if (curJSON[x].type == "vgroup") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);    
                            var group = Vgroup(parentDiv);
                            parseFaustUI(curJSON[x].items, group);
                        } else if (curJSON[x].type == "hslider") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var hslider = Hslider(parentDiv, curJSON[x]);
                        } else if (curJSON[x].type == "vslider") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var vslider = Vslider(parentDiv, curJSON[x]);
                        } else if (curJSON[x].type == "nentry") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var nentry = Nentry(parentDiv, curJSON[x]);
                        } else if (curJSON[x].type == "knob") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var knob = Knob(parentDiv, curJSON[x]);
                        } else if (curJSON[x].type == "button") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var button = Button(parentDiv, curJSON[x]);
                        } else if (curJSON[x].type == "checkbox") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var checkbox = Checkbox(parentDiv, curJSON[x]);
                        } else if (curJSON[x].type == "hbargraph") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var hbargraph = Hbargraph(parentDiv, curJSON[x]);
                        } else if (curJSON[x].type == "vbargraph") {
                            parentDiv = createUIElement(curDiv, curJSON[x]);
                            var vbargraph = Vbargraph(parentDiv, curJSON[x]);
                        }
                    }
                }
                depth -= 1;
                if(depth % 2 == 1){
                    defaultColor = 'var(--light-gray)';
                } else {
                    defaultColor = 'var(--lighter-gray)';
                }
            }

        }

        // window.addEventListener("load", document.body.appendChild(FaustUI()));
        window.addEventListener("onload", FaustUI());
    }