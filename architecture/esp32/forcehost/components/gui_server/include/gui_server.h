#ifndef GUI_SERVER_H
#define GUI_SERVER_H


#include <stdio.h>
#include <string.h>
#include <sys/param.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include <dirent.h>

#include "esp_err.h"
#include "esp_log.h"

#include "esp_vfs.h"
#include "esp_spiffs.h"
#include "esp_http_server.h"

#include "websocket_server.h"
#include <functional>
#include <string.h>
#include <map>

typedef std::function < void(char*, uint64_t) > CallbackFunc_t;
typedef std::map < int, CallbackFunc_t > callbackMap;

static callbackMap cbMap;
static QueueHandle_t client_queue;
const static int client_queue_size = 10;

class WSReciever
{
private:

	static void websocket_callback(uint8_t num, WEBSOCKET_TYPE_t type, char* msg, uint64_t len) {
		const static char* TAG = "websocket_callback";
		//int value;
		//float float_value;

		switch (type) {
		case WEBSOCKET_CONNECT:
			ESP_LOGI(TAG, "client %i connected!", num);
			break;
		case WEBSOCKET_DISCONNECT_EXTERNAL:
			ESP_LOGI(TAG, "client %i sent a disconnect message", num);
			break;
		case WEBSOCKET_DISCONNECT_INTERNAL:
			ESP_LOGI(TAG, "client %i was disconnected", num);
			break;
		case WEBSOCKET_DISCONNECT_ERROR:
			ESP_LOGI(TAG, "client %i was disconnected due to an error", num);
			break;
		case WEBSOCKET_TEXT:
			if (len) { // if the message length was greater than zero
				switch (msg[0]) {
				case '\'':
					ESP_LOGI(TAG, "got SQ message length %i: %s", (int)len - 1, &(msg[0]));
					{
						auto itr = cbMap.find(static_cast<int>(WEBSOCKET_TEXT));
						if (itr != cbMap.end()) {
							(itr->second)(msg, len);
						}
					}
					break;
				default:
					ESP_LOGI(TAG, "got an unknown message with length %i", (int)len);
					break;
				}
			}
			break;
		case WEBSOCKET_BIN:
		{
			ESP_LOGI(TAG, "client %i sent binary message of size %i: \n", num, (uint32_t)len);
			auto itr = cbMap.find(static_cast<int>(WEBSOCKET_BIN));
			if (itr != cbMap.end()) {
				(itr->second)(msg, len);
			}
		}
		break;
		case WEBSOCKET_PING:
			ESP_LOGI(TAG, "client %i pinged us with message of size %i:\n%s", num, (uint32_t)len, msg);
			break;
		case WEBSOCKET_PONG:
			ESP_LOGI(TAG, "client %i responded to the ping", num);
			break;
		}
	}

	static void http_serve(struct netconn *conn) {
		const static char* TAG = "http_server";
		const static char HTML_HEADER[] = "HTTP/1.1 200 OK\nContent-type: text/html\n\n";
		const static char ERROR_HEADER[] = "HTTP/1.1 404 Not Found\nContent-type: text/html\n\n";
		const static char JS_HEADER[] = "HTTP/1.1 200 OK\nContent-type: text/javascript\n\n";
		const static char CSS_HEADER[] = "HTTP/1.1 200 OK\nContent-type: text/css\n\n";
		const static char JSON_HEADER[] = "HTTP/1.1 200 OK\nAccess-Control-Allow-Origin: *\n\n";
		//const static char PNG_HEADER[] = "HTTP/1.1 200 OK\nContent-type: image/png\n\n";
		const static char ICO_HEADER[] = "HTTP/1.1 200 OK\nContent-type: image/x-icon\n\n";
		//const static char PDF_HEADER[] = "HTTP/1.1 200 OK\nContent-type: application/pdf\n\n";
		//const static char EVENT_HEADER[] = "HTTP/1.1 200 OK\nContent-Type: text/event-stream\nCache-Control: no-cache\nretry: 3000\n\n";
		struct netbuf* inbuf;
		static char* buf;
		static uint16_t buflen;
		static err_t err;

		// default page
		extern const unsigned char gui_start[] asm("_binary_index_html_start");
		extern const unsigned char gui_end[]   asm("_binary_index_html_end");
		const size_t gui_len = (gui_end - gui_start);

		// favicon.ico
		//extern const uint8_t favicon_ico_start[] asm("_binary_favicon_ico_start");
		//extern const uint8_t favicon_ico_end[] asm("_binary_favicon_ico_end");
		//const uint32_t favicon_ico_len = favicon_ico_end - favicon_ico_start;

		// favicon.ico
		extern const uint8_t favicon_png_start[] asm("_binary_favicon_png_start");
		extern const uint8_t favicon_png_end[] asm("_binary_favicon_png_end");
		const uint32_t favicon_png_len = favicon_png_end - favicon_png_start;

		// faustui.css
		extern const uint8_t faustui_css_start[] asm("_binary_faustui_css_start");
		extern const uint8_t faustui_css_end[] asm("_binary_faustui_css_end");
		const uint32_t faustui_css_len = faustui_css_end - faustui_css_start;

		// faustui.js
		extern const uint8_t faustui_js_start[] asm("_binary_bundle_js_start");
		extern const uint8_t faustui_js_end[] asm("_binary_bundle_js_end");
		const uint32_t faustui_js_len = faustui_js_end - faustui_js_start;

		// error page
		extern const uint8_t error_html_start[] asm("_binary_error_html_start");
		extern const uint8_t error_html_end[] asm("_binary_error_html_end");
		const uint32_t error_html_len = error_html_end - error_html_start;

		// json
		extern const uint8_t ui_json_start[] asm("_binary_ui_json_start");
		extern const uint8_t ui_json_end[] asm("_binary_ui_json_end");
		const uint32_t ui_json_len = ui_json_end - ui_json_start;


		netconn_set_recvtimeout(conn, 5000); // allow a connection timeout of 1 second
		ESP_LOGI(TAG, "reading from client...");
		err = netconn_recv(conn, &inbuf);
		ESP_LOGI(TAG, "read from client");
		if (err == ERR_OK) {
			netbuf_data(inbuf, (void**)&buf, &buflen);
			if (buf) {

				// default page
				if     (strstr(buf, "GET / ")
				        && !strstr(buf, "Upgrade: websocket")) {
					ESP_LOGI(TAG, "Sending /");
					netconn_write(conn, HTML_HEADER, sizeof(HTML_HEADER) - 1, NETCONN_NOCOPY);
					netconn_write(conn, gui_start, gui_len, NETCONN_NOCOPY);
					netconn_close(conn);
					netconn_delete(conn);
					netbuf_delete(inbuf);
				}

				// default page websocket
				else if (strstr(buf, "GET / ")
				         && strstr(buf, "Upgrade: websocket")) {
					ESP_LOGI(TAG, "Requesting websocket on /");
					ws_server_add_client(conn, buf, buflen, (char *) "/", websocket_callback);
					netbuf_delete(inbuf);
				}

				else if (strstr(buf, "GET /bundle.js")) {
					ESP_LOGI(TAG, "Sending /bundle.js");
					netconn_write(conn, JS_HEADER, sizeof(JS_HEADER) - 1, NETCONN_NOCOPY);
					netconn_write(conn, faustui_js_start, faustui_js_len, NETCONN_NOCOPY);
					netconn_close(conn);
					netconn_delete(conn);
					netbuf_delete(inbuf);
				}

				else if (strstr(buf, "GET /faustui.css")) {
					ESP_LOGI(TAG, "Sending /faustui.css");
					netconn_write(conn, CSS_HEADER, sizeof(CSS_HEADER) - 1, NETCONN_NOCOPY);
					netconn_write(conn, faustui_css_start, faustui_css_len, NETCONN_NOCOPY);
					netconn_close(conn);
					netconn_delete(conn);
					netbuf_delete(inbuf);
				}

				else if (strstr(buf, "GET /favicon.ico")) {
					ESP_LOGI(TAG, "Sending favicon.ico");
					netconn_write(conn, ICO_HEADER, sizeof(ICO_HEADER) - 1, NETCONN_NOCOPY);
					netconn_write(conn, favicon_png_start, favicon_png_len, NETCONN_NOCOPY);
					netconn_close(conn);
					netconn_delete(conn);
					netbuf_delete(inbuf);
				}

				else if (strstr(buf, "GET /JSON")) {
					ESP_LOGI(TAG, "Sending JSON");
					netconn_write(conn, JSON_HEADER, sizeof(JSON_HEADER) - 1, NETCONN_NOCOPY);
					netconn_write(conn, ui_json_start, ui_json_len, NETCONN_NOCOPY);
					netconn_close(conn);
					netconn_delete(conn);
					netbuf_delete(inbuf);
				}

				else if (strstr(buf, "GET /")) {
					ESP_LOGI(TAG, "Unknown request, sending error page: %s", buf);
					netconn_write(conn, ERROR_HEADER, sizeof(ERROR_HEADER) - 1, NETCONN_NOCOPY);
					netconn_write(conn, error_html_start, error_html_len, NETCONN_NOCOPY);
					netconn_close(conn);
					netconn_delete(conn);
					netbuf_delete(inbuf);
				}

				else {
					ESP_LOGI(TAG, "Unknown request");
					netconn_close(conn);
					netconn_delete(conn);
					netbuf_delete(inbuf);
				}
			}
			else {
				ESP_LOGI(TAG, "Unknown request (empty?...)");
				netconn_close(conn);
				netconn_delete(conn);
				netbuf_delete(inbuf);
			}
		}
		else { // if err==ERR_OK
			ESP_LOGI(TAG, "error on read, closing connection");
			netconn_close(conn);
			netconn_delete(conn);
			netbuf_delete(inbuf);
		}
	}

public:
	const int msgMaxSize = 1024;
	void setCallback(void* obj, WEBSOCKET_TYPE_t type, std::function<void(void *, char*, uint64_t)> callback) {
		auto f = std::bind(callback, obj, std::placeholders::_1, std::placeholders::_2);
		cbMap.emplace(static_cast<int>(type), f);
	}

	// handles clients when they first connect. passes to a queue
	static void client_queue_task(void* pvParameters) {
		const static char* TAG = "client_queue_task";
		struct netconn *conn, *newconn;
		static err_t err;
		client_queue = xQueueCreate(client_queue_size, sizeof(struct netconn*));

		conn = netconn_new(NETCONN_TCP);
		netconn_bind(conn, NULL, 80);
		netconn_listen(conn);
		ESP_LOGI(TAG, "server listening");
		do {
			err = netconn_accept(conn, &newconn);
			ESP_LOGI(TAG, "new client");
			if (err == ERR_OK) {
				xQueueSendToBack(client_queue, &newconn, portMAX_DELAY);
				// http_serve(newconn);
			}
		} while (err == ERR_OK);
		netconn_close(conn);
		netconn_delete(conn);
		ESP_LOGE(TAG, "task ending, rebooting board");
		esp_restart();
	}

// receives clients from queue, handles them
	static void server_task(void* pvParameters) {
		const static char* TAG = "server_task";
		struct netconn* conn;
		ESP_LOGI(TAG, "task starting");
		for (;;) {
			xQueueReceive(client_queue, &conn, portMAX_DELAY);
			if (!conn) continue;
			http_serve(conn);
		}
		vTaskDelete(NULL);
	}

};

#endif