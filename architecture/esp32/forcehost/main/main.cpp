/*

        Copyright (C) 2020, Mathias Kirkegaard, IDMIL

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include <iostream>

#include "esp_event.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "esp_spi_flash.h"
#include "esp_spiffs.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "lwip/api.h"
#include "multi_heap.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"
// #include "esp_heap_trace.h"

#include <cstring>

#include "AudioDriver.h"
#include "esp32.h"

// #define RUNTIME_STATS

#ifdef CHECK_MEMORY
#define NUM_RECORDS 200
static heap_trace_record_t trace_record[NUM_RECORDS];
#endif

#ifdef RUNTIME_STATS
static char runtime_stats_buffer[500];
#endif

const int SR = 32000;
const int BS = 32;

extern "C" {
void app_main(void);
}
/* Function to initialize SPIFFS */
static esp_err_t init_spiffs(void) {
    static const char *TAG = "SPIFFS";
    ESP_LOGI(TAG, "Initializing SPIFFS");

    esp_vfs_spiffs_conf_t conf = {
        .base_path = "/spiffs", .partition_label = NULL, .max_files = 7, .format_if_mount_failed = true};

    esp_err_t ret = esp_vfs_spiffs_register(&conf);
    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return ESP_FAIL;
    }

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
        return ESP_FAIL;
    }

    ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    return ESP_OK;
}

void app_main(void) {
#ifdef CHECK_MEMORY
    ESP_ERROR_CHECK(heap_trace_init_standalone(trace_record, NUM_RECORDS));
#endif

    // Disable WiFi power save (huge latency improvements)
    esp_wifi_set_ps(WIFI_PS_NONE);

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in
     * menuconfig. Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());

    /* Initialize file storage */
    ESP_ERROR_CHECK(init_spiffs());

    static const char *MAIN_APP = "MAIN APP";

    AudioDriver driver;

    const char *err = esp_err_to_name(driver.Init(SR, 32));
    // vTaskDelay(1000 / portTICK_PERIOD_MS); // wait for driver init to complete
    ESP_LOGI(MAIN_APP, "Audio driver I2C setup: %s ", err);

    AudioFaust *DSP = new AudioFaust(SR, BS);
    DSP->start();
#ifdef CHECK_MEMORY
    ESP_ERROR_CHECK(heap_trace_start(HEAP_TRACE_ALL));
#endif
    while (1) {
        vTaskDelay(5000 / portTICK_PERIOD_MS);
#ifdef CHECK_MEMORY
        if (!heap_caps_check_integrity_all(true)) {
            printf("error in heap\n");
            ESP_ERROR_CHECK(heap_trace_stop());
            heap_trace_dump();
        }
        ESP_LOGI(MAIN_APP, "Free  memory is: %i ", heap_caps_get_free_size(MALLOC_CAP_8BIT));
#endif
#ifdef RUNTIME_STATS
        // vTaskGetRunTimeStats(runtime_stats_buffer);
        vTaskList(runtime_stats_buffer);
        std::cout << runtime_stats_buffer << std::endl;
#endif
        // printf("Angle is %f \n", DSP->getHapticInput());
        // printf("Torque is %f \n", DSP->getHapticOutput());
    }
}
