/*
	AudioDriver - abstract driver library for ESP-IDF

    See drivers in architecture/esp32/drivers/

	Copyright (C) 2020, Christian Frisson, IDMIL

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __AudioDriver_H__
#define __AudioDriver_H__

class AudioDriver
{
public:

	/**
	* @brief Initialize codec chip
	*
	* @return
	*     - ESP_OK
	*     - ESP_FAIL
	*/
	esp_err_t Init() {
	    return ESP_OK;
    }

};

#endif //__AudioDriver_H__