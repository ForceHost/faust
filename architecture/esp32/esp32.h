/************************************************************************
 FAUST Architecture File
 Copyright (C) 2019-2020 GRAME, Centre National de Creation Musicale &
 Aalborg University (Copenhagen, Denmark)
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.

 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.

 ************************************************************************/

#ifndef faust_esp32_h_
#define faust_esp32_h_

#include <string>

#include "driver/i2s.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

class dsp;
class esp32audio;
class MapUI;
class MetaDataUI;
class GUI;

#ifdef WS_GUI
class Esp32WebsocketUI;
#endif /* WS_GUI */

#ifdef LIBMAPPER_UI
class Esp32LibmapperUI;
#endif /* LIBMAPPER_UI */

#ifdef HAPTICS
class HapticController;
#endif /* HAPTICS */

#ifdef MIDICTRL
class MidiUI;
class esp32_midi;
#endif /* MIDICTRL */

class AudioFaust {
   private:
    esp32audio* fAudio;
    dsp*        fDSP;
    MapUI*      fUI;
#ifdef LIBMAPPER_UI
    Esp32LibmapperUI* fLibmapperUI;
#endif /* HAPTICS*/

#ifdef WS_GUI
    Esp32WebsocketUI* fGUI;
#endif /* HAPTICS*/

#ifdef HAPTICS
    HapticController* fHapticCtrl;
#endif /* HAPTICS*/

#ifdef MIDICTRL
    esp32_midi* fMIDIHandler;
    MidiUI*     fMIDIInterface;
#endif

   public:
    AudioFaust(int sample_rate, int buffer_size);
    ~AudioFaust();

    bool  start();
    void  stop();
    float getHapticInput();
    float getHapticOutput();

    void  setParamValue(const std::string&, float);
    float getParamValue(const std::string&);
};

#endif /* faust_esp32_h_ */
